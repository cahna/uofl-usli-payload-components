classdef rec_weather
    %REC_CONTROL Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        location
        vec
        dist = 'gaussian';
        mean = [0 0 0]
        var  = [1 1 1]
    end
    
    methods

        function vec = get.vec(obj)
            if strcmp(obj.dist,'gaussian')
                vec = normrnd(obj.mean,obj.var);
            end
        end
        
    end
    
end