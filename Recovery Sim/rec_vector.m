%
%  University of Louisville USLI, 2011-2012
%
%  Filename		: rec_vector.m
%  Written by	: Luke Spicer[, Nick Searcy]
%  Date		: 17 Decemeber 2011
%  Description	: This is a simple object for controlling spacial vectors.
%   It may be unnecessary.
%  [Dependencies	: (none)]

classdef rec_vector
    % uses a 1 by X vector for vector math
    %   Detailed explanation goes here
    
    properties
        vec
        mag
        u
        angle %angle in xy plane from vector in degrees
    end
    
    methods
        
        function obj = set.vec(obj,vector)
            if size(vector(1)) ~= 1
                error('input vectors must be 1 by X')
            end
            obj.vec = vector;
        end %set vec
        
        function mag = get.mag(obj)
            mag = sqrt(sum(obj.vec.^2));
        end %get mag
        
        function u = get.u(obj)
            u = obj.vec./obj.mag;
        end % get u
        
        function angle = get.angle(obj)
            angle = (180/pi)*atan2(obj.u(2),obj.u(1)); 
                %angle in xy plane from vector in degrees
        end %get angle
        
        function unit = rotate(obj,theta)
            if nargin ~= 2
                error('Yo, I just need one angle. Nothing else')
            end%if
            unit = obj.u(1:2)*[cos(pi*theta/180) sin(pi*theta/180);...
                -sin(pi*theta/180) cos(pi*theta/180)];
        end%rotate
        
    end
    
end