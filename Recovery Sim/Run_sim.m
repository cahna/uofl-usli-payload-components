clear all
clc
format compact
close all

v = rec_sim;
v.theta_set = -25; %set control angle
v.r_min = 50; %set minimum turning radius
v.weather.mean = [0 0 -5];
v.weather.var = [2 2 2];

v.plot_sim(10)