classdef rec_sim
    %REC_SIM Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        theta_set = -25;
        r_min = 50;
        unit_dist = 'ft';
        unit_angle = 'degree';
        sim_path
        ideal_sim_path
        weather = rec_weather;
    end
    
    methods
        %build a single simulated path 
        function sim_path = get.sim_path(obj)
            vh = rec_vehicle;
            cn = rec_control;
            cn.theta_set = obj.theta_set;
            cn.r_min = obj.r_min;
            while vh.path(vh.i,3) > 0
                cn.theta = vh.theta;
                vh.l = cn.l;
                vh = vh.step(obj.weather.vec);
            end
            sim_path = vh.path;
            
        end % get sim path
        
        function plot_sim(obj,iterations)

            figure(1)
            
            fu = rec_vehicle;
            z = fu.z_min;
            
            % generate the cylinder of minimum turning radius
            [cyl_X,cyl_Y,cyl_Z] = cylinder(obj.r_min);
            cyl_Z = cyl_Z*z;

            % plot everything on figure 1
            mesh(cyl_X,cyl_Y,cyl_Z)% plot the cylinder
            hold on
            grid on
            rotate3d on

            % plot cirucles of various radii for comparing curves to
            theter = 0:pi/50:2*pi;
            r = 50;
            for i = 1:z/250
                [circle_X, circle_Y] = pol2cart(theter, r);
                plot(circle_X, circle_Y,'g');
                r = i*250;
            end%for

            for i = 1:iterations
                path = obj.sim_path;
                plot3(path(:,1),path(:,2),path(:,3))
            end%for
            xlabel('x')
            ylabel('y')
            zlabel('z')
            
        end%plot sim

    end % methods
    
end % rec sim

