classdef rec_control
    %REC_CONTROL Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        theta_set           %goal theta set by user
        theta
<<<<<<< HEAD
        i
        pK = .8/100;
        iK = .01/100;
        dK = .7/100;
        delta_l             %change in line length
        prev_l = 0;
        l                   %line length (% of keel length)
=======
        l
        r                   %change in radius of rotation
>>>>>>> origin/HEAD
        r_min = 50;
    end
    
    methods
        
%         function r = get.r(obj)
%             error = obj.theta - obj.theta_set;
%             pK = 400;
%             r = (1/error)*pK;
%             if (abs(r) < obj.r_min)
%                 r = obj.r_min;
%             end
%         end
        
        function r = get.r(obj)
            error = obj.theta - obj.theta_set;
            if error > 0
                r = 50;
            else
                r = -50;
            end
        end

        function l = get.l(obj)
            l = sign(obj.r)*0.05*(sqrt(abs(obj.r)^2 ...
                - 120*abs(obj.r) + 4000) - abs(obj.r) + 60);
        end
        
    end
    
end

