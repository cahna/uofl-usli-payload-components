classdef rec_vehicle
    %this is the rocket paraglider
    %   get the fuck ready
    
    properties
        z_rate = 6.56;          %falls at 2m/sec = 6.56 ft/sec
        xy_rate                 %moves along ground at 3x z_rate
        path                    %history of locations
        r
        l = eps;            %radius of rotation
        next_pos                %simulate displacement based on r
        i = 1;                  %Current position at obj.path(obj.i,:)
    end % vanilla properties
    
    properties
        goal = [0 0];           %goal location: x y z
        x_min = 2000;            %starting location is a random
        x_max = 2500;           %distance in ft
        z_min = 1700;           %x is lateral distance
        z_max = 2000;           %z altitude
        COG_min = -5;           %COG is starting speed in xy direction
        COG_max = 5;            %in ft/s
        resolution = 1000;      %time between steps
        unit_dist = 'ft';       %distance in ft
        unit_time = 'sec';      %time in ms
    end % location parameters
    
    properties (Dependent)
        COG                     %vector in direction of xy velocity
        home                    %vector in direction of goal location
        theta                   %Current angle between Home and COG
    end %dependent 
        
        
    methods % constructor
        function vh = rec_vehicle(x_min,x_max,z_min,z_max,z_rate,resolution)

            %set arguments
            if nargin > 0
                vh.x_min = x_min;
            end%0
            if nargin > 1
                vh.x_max = x_max;
            end%1
            if nargin > 2
                vh.z_min = z_min;
            end%2
            if nargin > 3
                vh.z_max = z_max;
            end%3
            if nargin > 4
                vh.z_rate = z_rate;
            end%4
            if nargin > 5
                vh.resolution = resolution;
            end%5
            
            %randomly determine distance in xy-plane (x) and altitude (z)
            %fix the path length so that path does not grow in loop
            x = vh.x_min + (vh.x_max - vh.x_min).*rand(1,1);
            z = vh.z_min + (vh.z_max - vh.z_min).*rand(1,1);
            steps = (1000/vh.resolution)*(z/vh.z_rate);
            vh.path = zeros(round(1.1*steps),3); %give extra room because wind
            vh.path(vh.i,:) = [x 0 z];
            
            %set initial direction and second location
            COG1 = rec_vector;
            COG1.vec = vh.COG_min + (vh.COG_max - vh.COG_min).*rand(1,2);
            vh.xy_rate = 3*vh.z_rate;
            vh.i = 2;
            vh.path(vh.i,:) = vh.path(vh.i-1 ,:) + (vh.resolution/1000)...
                *[vh.xy_rate.*COG1.u vh.z_rate*(-1)];
        end %constructor
    end % method

    methods
        
        function COG = get.COG(obj)
                COG = rec_vector;
            if obj.i > 1
                COG.vec = obj.path(obj.i,1:2) - obj.path(obj.i-1,1:2);
            else
                COG.vec = [0 0];
            end%if
        end %get COG 
        
        function home = get.home(obj)
            home = rec_vector;
            home.vec = obj.goal - obj.path(obj.i,1:2);
        end %get home
        
        function theta = get.theta(obj)
            t = obj.COG.angle - obj.home.angle;
            if t > 180
                theta = t - 360;
            elseif t <= -180
                theta = 360 - t;
            else
                theta = t;
            end
        end%if        end %get theta
        
        function r = get.r(obj)
            if obj.l == 0 %watch out for divide by zero error
                r = realmax; %r is very large when obj.l = 0
            else
                r = (10/abs(obj.l) - 10*abs(obj.l) + 60);
            end%if
        end
        
        function obj = step(obj,weather)
            % Simulate Position based on r from control algorithm
            % first find the center of circle of rotation
            
            turn = -sign(obj.l);  %turn right for positive l, left  
                                  %   for negative l
            vr = rec_vector;
            vr.vec = obj.COG.rotate(turn*90);   %center of circle is in the 
                                                %direction of COG rotated  
                                                %90 degrees
            c = obj.path(obj.i,1:2) + vr.u*obj.r; %center is r distance
                                                  % from current location
            phi = 360*(obj.xy_rate/(2*pi*obj.r));   %angle of arc of 
                                                    %rotation
            obj.path(obj.i+1,1:2) = c + obj.r*vr.rotate(turn*(phi - 180))...
                + weather(1:2);
            obj.path(obj.i+1,3) = obj.path(obj.i,3) - obj.z_rate...
                + weather(3);
            obj.i = obj.i + 1;
            
        end% step
        
    end % methods
    
end % rec body