--[[
@title USLI Minimal Intervalometer
@param a Initial Delay, min
@default a 5
@param b Shoot Interval, sec
@default b 5
--]]
 
Delay = a*60000
Interval = b*1000

function TakePicture()
	press("shoot_half")
    repeat sleep(50) until get_shooting() == true
	press("shoot_full")
	release("shoot_full")
	repeat sleep(50) until get_shooting() == false	
    release "shoot_half"
end

sleep(Delay)

repeat
	StartTick = get_tick_count()
	TakePicture()
	sleep(Interval - (get_tick_count() - StartTick))
until false