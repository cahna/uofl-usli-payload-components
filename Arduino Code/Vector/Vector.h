/* 
 * File:   Vector.h
 * Author: Adrian
 *
 * Created on January 27, 2012, 6:03 PM
 */

#ifndef VECTOR_H
#define	VECTOR_H

class Vector {
    
public:
    Vector(double angle);
    Vector(double x, double y);
    
    double getAngle();
    double getMagnitude();
    double getX();
    double getY();
    void setAngle(double angle);
    void setMagnitude(double magnitude);
    void setXY(double x, double y);
    
    void rotate(double theta);
    double cmpAngle(Vector* cmpVector);
    
private:
    double angle;
    double magnitude;
    double x;
    double y;
    void update(int type);
};

#endif	/* VECTOR_H */

