/* 
 * File:   Vector.cpp
 * Author: Adrian
 * 
 * Created on January 27, 2012, 6:03 PM
 */

#include "Vector.h"
#include <math.h>

#define PI 3.14159265

Vector::Vector(double angle)
{
    this->angle = fmod(angle,360);
    if(this->angle > 180)
    {
        this->angle -= 360;
    }
    else if(this->angle < -180)
    {
        this->angle += 360;
    }
    magnitude = 1;
    x=cos(this->angle*PI/180);
    y=sin(this->angle*PI/180);
}

Vector::Vector(double x, double y)
{
    this->x = x;
    this->y = y;
    magnitude = sqrt(pow(x,2)+pow(y,2));
    angle = atan(y/x) * 180 / PI;
    
    if((x < 0) && (y>0))
    {
        angle += 180;
    }
    else if((x < 0) && (y < 0))
    {
        angle -= 180;
    }
}

double Vector::getAngle()
{
    return angle;
}

double Vector::getMagnitude()
{
    return magnitude;
}

double Vector::getX()
{
    return x;
}

double Vector::getY()
{
    return y;
}

void Vector::setAngle(double angle)
{
    this->angle = angle;
    update(1);
}

void Vector::setMagnitude(double magnitude)
{
    this->magnitude = magnitude;
    update(1);
}

void Vector::setXY(double x, double y)
{
    this->x = x;
    this->y = y;
    update(2);
}
    
void Vector::rotate(double theta)
{
    angle = fmod(angle+theta,360);
    if(angle > 180)
    {
        angle -= 360;
    }
    else if(angle < -180)
    {
        angle += 360;
    }
}

double Vector::cmpAngle(Vector* cmpVector)
{
    double diff = cmpVector->angle - this->angle;
    if(diff > 180)
    {
        diff -= 360;
    }
    else if(diff < -180)
    {
        diff += 360;
    }
    return diff;
}

void Vector::update(int type)
{
    switch(type)
    {
        case 1:
            x=magnitude*cos(angle*PI/180);
            y=magnitude*sin(angle*PI/180);
            break;
            
        case 2:
            angle = atan(y/x) * 180 / PI;

            if((x < 0) && (y>0))
            {
                angle += 180;
            }
            else if((x < 0) && (y < 0))
            {
                angle -=180;
            }
            break;
    }
}