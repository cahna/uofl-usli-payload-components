//
//  University of Louisville USLI, 2011-2012
//
//  Filename    :  VinMonitor_example1
//  Written by  :  Lucas Spicer
//  Date        :  13 November 2011
//  Description :  Arduino Example Sketch of the VinMonitor library
//                 This library is written for to allow an Arduino based device
//                 to measure the Vin pin (and indirectly the pre-regulated input/battery
//                 voltage). All that is requiredis a resistor voltage divider consisting of two
//                 nominally equivilent resitors of nominal value between about 10k and 100k ohms.
//                 The resistor divider should be placed between Vin and ground, with the mid-node
//                 between the resistors input to the analog input pin
//

// Include the Library we wish to use in this example
#include <VinMonitor.h>
 
// Create Global Objects
 
// Create Variables to hold the values of the analog pin we wish to connect the VinMonitor to
// as well as the exact measured values of Vref, the resistors used in the divider and the
// Vin protection diod's voltage drop. (Including the diode drop allows the Vin monitor to
// return the batteries output voltage)
// It is not necessary to create variables to hold these values, you could just pass
// the values themselves to the constructor, but this method is used in the example for illustrative
// purposes
int Vin_analog_input_pin = 0;
float Vref = 5.04;
float Rref = 32.60;
float Rup = 32.71;
float diode_drop = 0.595;
 
// Create an instance of our VinMonitor class (name of this object is battery, but could be anything)
// Multiple instances of the VinMonitor device can be created in one sketch, but since there is only
// one Vin pin on each Arduino there may be little point to do this
VinMonitor battery(Vin_analog_input_pin, Vref, Rref, Rup, diode_drop);

// All Arduino Sketches require both a setup() and loop() funtions, this is the setup()
void setup() {
  // Initialize the Serial port (the one connected to the USB interface) at 9600 for debugging and
  // receiving message on the computer's serial monitor
  Serial.begin(9600);
  // A friendly message to begin our test!
  Serial.println("Battery Voltage Monitor Test\n"); 
}

// All Arduino Sketches require both a setup() and loop() funtions, this is the loop() 
void loop() {
 // Read the Vin pin and print the result on the Serial Monitor every 1000mSec
 Serial.print("Battery Voltage: ");
 Serial.print(battery.get_Vin());
 Serial.println(" V");
 delay(1000);
}
