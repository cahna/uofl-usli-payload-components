//
// University of Louisville USLI, 2011-2012
//
// Filename		:	VinMonitor.h
// Written by	:	Lucas Spicer
// Date			:	13 November 2011
// Description 	:	C++ Header file which contains a class defintion for a custom
//					VinMonitor class (which is designed to allow a specified analog input
//					on the arduino measure the Vin (essentially the battery voltage to the system)
//					across a precision (or precisely measured) dual resistor voltage divider
//

#ifndef Vin_h
#define Vin_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

class VinMonitor
{
	public:
		// VinMonitor object Constructors
		VinMonitor(int pin);
		VinMonitor(int pin, float Vref, float Rref, float Rup, float diode);
		// PUBLIC FUNCTIONS
		float get_Vin(void);
		float get_Vref(void);
		float get_diode_drop(void);
		float get_Rref(void);
		float get_Rup(void);
		int get_pin(void);
		
		void set_Vref(float Vref);
		void set_diode_drop(float diode);
		void set_Rref(float Rref);
		void set_Rup(float Rup);
		void set_pin(int pin);
	private:
		// PRIVATE FUNCTIONS
		float _Vref;
		float _diode_drop;
		float _Rref;
		float _Rup;
		int _pin;
};

#endif