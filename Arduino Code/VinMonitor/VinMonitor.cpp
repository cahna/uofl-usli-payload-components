//
// University of Louisville USLI, 2011-2012
//
// Filename		:	VinMonitor.cpp
// Written by	:	Lucas Spicer
// Date			:	13 November 2011
// Description 	:	C++ file which contains function definitions for the custom
//					VinMonitor class. VinMonitor constructor connects the object to the analog
//					input pin and sets the various voltage levels to get accurate readings
//					of the Vin pin (the battery voltage) attached to the arduino
//

#include "VinMonitor.h"

//****** PUBLIC//******//

//
// Simple VinMonitor constructor VinMonitor()
//
// Input:	int pin, integer number of analog input pin used to measure Vin
// Return:	N Applicable
//
// Description: Creates an input voltage monitor object
//
VinMonitor::VinMonitor(int pin) {
  _pin = pin;			// analog pin used for measurment
  _Vref = 5.0;		// voltage of 5V rail used for Vref
  _Rref = 10;			// Lower resistor of resistor divider in k Ohms
  _Rup = 10;			// Upper resistor of resistor divider in k Ohms
  _diode_drop = 0.0;	// Voltage drop of protection diode in arduio 5v circuit
}

//	
// Full VinMonitor Constructor VinMonitor()
//
// Input: 	int pin (integer number of analog input pin used to measure Vin)
//			float Vref (float measured voltage level of arduiono 5V rail)
//			int Rref (integer value (in kOhms) of lower half of resitor divider)
//			int Rup (integer value (in kOhms) of uper half of resistor divider)
//			float diode (float measured voltage drop between Vbattery and Vin)
//				caused by protection diode in Arduino supply circuit
// Return:	Not Applicable
//
// Description: Creates an input voltage monitor object, but with increased accuracy available
//				passing the constructor the measured values for each resistor used in the divider
//				as well as the actual reference voltage value and the volage drop of the protection diode
//				(set this to 0 if you only want to know the voltage on the Vin pin and not the battery
//				voltage level)
//
VinMonitor::VinMonitor(int pin, float Vref, float Rref, float Rup, float diode) {
  _pin = pin;			// analog pin used for measurment
  _Vref = Vref;			// voltage of 5V rail used for Vref
  _Rref = Rref;			// Lower resistor of resistor divider in k Ohms
  _Rup = Rup;			// Uper resistor of resistor divider in k Ohms
  _diode_drop = diode;	// Voltage drop of protection diode in arduio 5v circuit
}

//	
// Function: get_Vin()
// 
// Input: 	void
// Return:	float voltage (float calculated value of Vin at source)
//
float VinMonitor::get_Vin(void) {
  float voltage = analogRead(_pin);
  voltage = voltage*_Vref/1024;
  voltage = voltage*(_Rup + _Rref)/_Rref;
  voltage = voltage + _diode_drop;
  return voltage;
}
	
//
// Function: get_Vref()
// 
// Input: 	void
// Return:	float _Vref, float value of Vref (arduino 5V rail)
//
float VinMonitor::get_Vref() {
  return _Vref;
}

//
// Function: get_diode_drop()
// 
// Input: 	void
// Return:	float _diodeDrop (float calculated value of protection diode voltage drop)
//
float VinMonitor::get_diode_drop() {
	return _diode_drop;
}

//
// Function: get_Rref()
// 
// Input: 	void
// Return:	float _Rref, float value of lower resistor in resistor divider (in kOhms)
//
float VinMonitor::get_Rref() {
	return _Rref;
}

//
// Function: get_Rup()
// 
// Input: 	void
// Return:	float _Rup, float value of upper resistor in resistor divider ( in kOhms)
//
float VinMonitor::get_Rup() {
	return _Rup;
}

//
// Function: get_pin()
// 
// Input: 	void
// Return:	int _pin, int value of analog input pin used to measure Vin
//
int VinMonitor::get_pin() {
	return _pin;
}

//
// Function: set_Vref()
// 
// Input: 	float Vref (float value of Vref (arduino 5V rail))
// Return:	void
//
void VinMonitor::set_Vref(float Vref) {
	_Vref = Vref;
}

//
// Function: set_diode_drop()
// 
// Input: 	float diode, float calculated value of protection diode voltage drop
// Return:	void
//
void VinMonitor::set_diode_drop(float diode) {
	_diode_drop = diode;
}

// Function: set_Rref()
// 
// Input: 	float Rref, float value of lower resistor in resistor divider
// Return:	void
//
void VinMonitor::set_Rref(float Rref) {
	_Rref = Rref;
}

//
// Function: set_Rup()
// 
// Input: 	float Rup, float value of upper resistor in resistor divider
// Return:	void
//
void VinMonitor::set_Rup(float Rup) {
	_Rup = Rup;
}

//
// Function: set_pin()
// 
// Input: 	int _pin, int value of analog input pin used to measure Vin
// Return:	void
//
void VinMonitor::set_pin(int pin) {
	_pin = pin;
}