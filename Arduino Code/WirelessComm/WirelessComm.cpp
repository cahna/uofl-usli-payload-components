//
// University of Louisville USLI, 2011-2012
//
// Filename        : GPS.cpp
// Written by      : Conor Heine, Lucas Spicer
// Date            : 18 February 2012
// Description     : The WirelessComm class controls the formatting of sensor
//                   data for wireless transmission to the ground station
//                   using the proper communication protocol. It is written to
//                   be compatible with the Arduino Mega board and the Arduino
//                   0.22 IDE.
//

#include "WirelessComm.h"

//
// Constructor: Sets the correct serial line. The serial port number should 
//              correspond to the lines connected to the Xbee wireless module.
//
WirelessComm::WirelessComm(int serialPortNumber){
  _serialPortNumber = serialPortNumber;
}

//
// Function beginSerial() : Begins serial communications at the BAUD rate 
//                          specified in WirelessComm.h
//
void WirelessComm::beginSerial(){
  switch(_serialPortNumber) {
    case 1:
      Serial1.begin(BAUD);
      break; 
    case 2:
      Serial2.begin(BAUD);
      break;
    case 3:
      Serial3.begin(BAUD);
      break;
    default:
      Serial.begin(BAUD);
      break;      
  }
}

//
// Function send() : Writes to the serial port defined when the object 
//                          was created
//
void WirelessComm::send(String packet){
	switch(_serialPortNumber) {
    case 1:
      Serial1.print(packet);
      break; 
    case 2:
      Serial2.print(packet);
      break;
    case 3:
      Serial3.print(packet);
      break;
    default:
      Serial.print(packet);
      break;      
  }
}

//
// Set up the variable name order (this should be the order that variables are passed
// to transmitData's float* vars)
//
String WirelessComm::_gpsVars[] = {"LATD","LATM","LATS","LOND","LONM","LONS","ALT","SPD","NSAT"};
String WirelessComm::_barVars[] = {"P","A","T"};
String WirelessComm::_humVars[] = {"H"};
String WirelessComm::_sirVars[] = {"S"};
String WirelessComm::_uvVars[]  = {"R"};
String WirelessComm::_rgbVars[] = {"R","G","B"};
String WirelessComm::_imuVars[] = {"XA","YA","ZA","XR","YR"};

//
// Function transmitData() : Formats the packet and sends it
//   Input:
//       timeStamp    : Time associated with the data to be send in the format of
//                      milliseconds since system start
//       sensorName   : Name of the sensor
//       sensorNumber : Indentifying number to indicate which science board the data
//                      is coming from (default to 0 for sensors that are not redundant)
//       vars         : Array of sensor information stored in the correct order as
//                      defined in the comments of WirelessComm.h
//
void WirelessComm::transmitData(unsigned long timeStamp, String sensorName, int sensorNumber, float* vars){
	// Send message header
	send("<{\"t\":");
	send(String(timeStamp));
	send(",\"s\":\"");
	send(sensorName);
	send("\",\"n\":");
	send(sensorNumber);
	send(",\"d\":{");
	
	// Format the data array
	sendVars(sensorName, vars);
	
	// Close the packet
	send("}}>\n");
}

//
// Function transmitImage() : Formats the image packet and sends it
//   Input:
//       timeStamp    : Time associated with the image to be send in the format of
//                      milliseconds since system start
//       picNumber    : Filename of picture (in terms of order taken since startup)
//       buffer       : JPG image data
//       length       : Length of the buffer
//
void WirelessComm::transmitImage(unsigned long timeStamp, int picNumber, uint8_t* buffer, uint8_t length){
	// Send message header
	send("<{\"t\":");
	send(String(timeStamp));
	send(",\"s\":\"");
	send("IMG");
	send("\",\"n\":");
	send(picNumber);
	send(",\"l\":");
	send(length);
	send(",\"d\":{\"JPG\":\"");
	
	// Send jpg data
	for(int n = 0; n < length; n++) {
		send(buffer[n]);
	}
	
	// Close the packet
	send("\"}}>\n");
}

//
// Function transmitFailure() : Sends a meaningful message that indicates 
//                              sensor failure with an assoicated timestamp
//   Input:
//       timeStamp    : Time associated with the failure of the specified sensor
//       sensorName   : Name of the sensor
//       sensorNumber : Indentifying number to indicate which science board the data
//                      is coming from (default to 0 for sensors that are not redundant)
//
void WirelessComm::transmitFailure(unsigned long timeStamp, String sensorName, int sensorNumber){
	// Send message header
	send("<{\"t\":");
	send(String(timeStamp));
	send(",\"s\":\"");
	send(sensorName);
	send("\",\"n\":");
	send(sensorNumber);
	send(",\"d\":{\"fail\":true}}");
}

//
// Function sendVars() : Formats the variables for the given sensorName
//   Input:
//       sensorName : Name of the sensor
//       vars       : Array of sensor information stored in the correct order as
//                    defined in the comments of WirelessComm.h
// 
void WirelessComm::sendVars(String sensorName, float* vars){
	String dataTemplate = "\"%var%\":%val%";
	
	// Control variables
	int numVars;         // Number of variables to be sent
	String* varNames;    // Pointer to array of variable names for the correct sensor
	
	// Set control variables
	if(sensorName == "GPS"){
		numVars = V_GPS;
		varNames = _gpsVars;
	} else if(sensorName == "BAR"){
		numVars = V_BAR;
		varNames = _barVars;
	} else if(sensorName == "HUM"){
		numVars = V_HUM;
		varNames = _humVars;
	} else if(sensorName == "SIR"){
		numVars = V_SIR;
		varNames = _sirVars;
	} else if(sensorName == "UV"){
		numVars = V_UV;
		varNames = _uvVars;
	} else if(sensorName == "RGB"){
		numVars = V_RGB;
		varNames = _rgbVars;
	} else if(sensorName == "IMU"){
		numVars = V_IMU;
		varNames = _imuVars;
	} else {
		return;
	}
	
	// Sends all key:value pairs for the arrays pointed to by varNames and vars
	for(int i = 0; i < numVars; i++){
		send("\"");
		send(varNames[i]);
		send("\":");
		
		// ***KEY***
		// dtostrf(FLOAT,WIDTH,PRECSISION,BUFFER);
		// converts a float to a character array, which can become a String object
		char buff[15];
		send(String(dtostrf(vars[i],9,5,buff)).trim());
		
		if(i != (numVars - 1))
			send(",");
	}
}