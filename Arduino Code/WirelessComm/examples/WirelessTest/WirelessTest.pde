#include <WirelessComm.h>
#define SERIALNUM 0

WirelessComm myWireless(SERIALNUM);
float gpsVars[9] = {84.023, 35.031, 22.0, 88.023, 30.031, 29.0, 102.3, 12.5, 7.3};
float barVars[3] = {128.023, 124, 23.4};
float humVars[1] = {55.2};
float sirVars[1] = {60.01232};
float uvVars[1]  = {11.19988};
float rgbVars[3] = {10, 20, 30};
float imuVars[5] = {0.2, 0.7, 20, 0.1, 0.3};

String gps = "GPS";
String bar = "BAR";
String hum = "HUM";
String sir = "SIR";
String uv  = "UV";
String rgb = "RGB";
String imu = "IMU";

uint8_t imgData[30] = {0x4947, 0x3846, 0x6139, 0x0004, 0x0004, 0x00A2, 0x0000, 0x0000, 
                       0xffff, 0xa0ff, 0xa0a0, 0x9898, 0x9498, 0x9494, 0x9090, 0x0190, 
                       0x0101, 0x0000, 0x2c00, 0x0000, 0x0000, 0x0004, 0x0004, 0x0300, 
                       0x0809, 0x4a5a, 0x2361, 0x2104, 0x0243, 0x3b00};

int counter;
int picNumber;
int iterations;
float randomOffset;

void setup() 
{
  myWireless.beginSerial();
  counter = 0;
  picNumber = 1;
  iterations = 0;
  randomSeed(analogRead(0));
}

void applyOffset(float* dataSet, int length, float offset)
{
  int i;
  for(i = 0; i < length; i++){
      dataSet[i] += offset;
  }
}

void loop()
{
  // Apply random offset to all sensor vars to simulate changing data
  randomOffset = random(-20.0, 20.0);
  applyOffset(gpsVars, 9, randomOffset);  
  myWireless.transmitData(millis(), gps, 0, gpsVars);
  
  randomOffset = random(-20.0, 20.0);
  applyOffset(barVars, 3, randomOffset);
  myWireless.transmitData(millis(), bar, counter, barVars);
  
  randomOffset = random(-20.0, 20.0);
  applyOffset(humVars, 1, randomOffset);
  myWireless.transmitData(millis(), hum, counter, humVars);
  
  randomOffset = random(-20.0, 20.0);
  applyOffset(sirVars, 1, randomOffset);
  myWireless.transmitData(millis(), sir, counter, sirVars);
  
  randomOffset = random(-20.0, 20.0);
  applyOffset(uvVars, 1, randomOffset);
  myWireless.transmitData(millis(), uv, counter, uvVars);
  
  randomOffset = random(-20.0, 20.0);
  applyOffset(rgbVars, 3, randomOffset);
  myWireless.transmitData(millis(), rgb, counter, rgbVars);
  
  randomOffset = random(-20.0, 20.0);
  applyOffset(imuVars, 5, randomOffset);
  myWireless.transmitData(millis(), imu, counter, imuVars);
  
  // Send image data
  if(iterations % 25 == 0){
    myWireless.transmitImage(millis(), picNumber, imgData, sizeof(imgData)*2);
    picNumber++;
  }
  
  // Simulate changing sensor numbers (science boards)
  if(counter >= 8)
    counter = 0;
  else
    counter++;
    
  iterations++;
}
