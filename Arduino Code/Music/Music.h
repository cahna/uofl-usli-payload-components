/* Music Library Header file
 * Luke Spicer, March 17th, 2009
 * Use this Libary to play music/tones with
 * a piezo element and your Arduino!
 */

#ifndef Music_h    //these make sure the libary is only called once
#define Music_h

#if defined(ARDUINO) && ARDUINO >= 100 //this includes the stdlib and other arduino files
#include "Arduino.h"
#else
#include "WProgram.h"
#endif      

/*=============================================================================
 * To Play music with the piezo we must use the frequency of each
 * note to define its period (that is how long each on/off cycle will last
 * for that particular note). The periods are in microseconds.
 * I have defined 6 octaves of notes, if you wish to add others or comment
 * out the ones you don't need, feel free to do so.
===============================================================================
====== note/period     frequency ==============*/
         
#define C1  30581   // 33  Hz  C
#define Cs1 28860   // 35  Hz  D flat / C sharp
#define D1  27241   // 37  Hz  D
#define Eb1 25714   // 38  Hz  D sharp / E flat
#define E1  24272   // 41  Hz  E
#define F1  22910   // 44  Hz  F
#define Fs1 21622   // 46  Hz  G flat / F sharp
#define G1  20408   // 49  Hz  G
#define Gs1 19264   // 52  Hz  A flat / G sharp
#define A1  18182   // 55  Hz  A
#define Bb1 17161   // 58  Hz  B flat
#define Bn1 16197   // 62  Hz  B (natural)

#define C2  15288   // 65  Hz  C
#define Cs2 14430   // 70  Hz  D flat / C sharp
#define D2  13620   // 73  Hz  D
#define Eb2 12857   // 78  Hz  D sharp / E flat
#define E2  12134   // 82  Hz  E
#define F2  11453   // 87  Hz  F
#define Fs2 10811   // 93  Hz  G flat / F sharp
#define G2  10204   // 98  Hz  G
#define Gs2 9631    // 104 Hz  A flat / G sharp
#define A2  9091    // 110 Hz  A
#define Bb2 8583    // 117 Hz  B flat
#define B2  8099    // 123 Hz  B

#define C3  7645    // 131 Hz  C
#define Cs3 7216    // 138 Hz  D flat / C sharp
#define D3  6811    // 147 Hz  D
#define Eb3 6428    // 156 Hz  D sharp / E flat
#define E3  6068    // 165 Hz  E
#define F3  5727    // 175 Hz  F
#define Fs3 5405    // 185 Hz  G flat / F sharp
#define G3  5102    // 196 Hz  G
#define Gs3 4816    // 208 Hz  A flat / G sharp 
#define A3  4545    // 220 Hz  A
#define Bb3 4290    // 233 Hz  B flat
#define B3  4050    // 246 Hz  B

#define c4  3822    // 261 Hz ===================MIDDLE C======================
#define cs4 3608    // 277 Hz  D flat / C sharp 
#define d4  3405    // 294 Hz  D 
#define eb4 3214    // 311 Hz  D sharp / E flat
#define e4  3034    // 329 Hz  E
#define f4  2863    // 349 Hz  F
#define fs4 2703    // 370 Hz  G flat / F sharp  
#define g4  2551    // 392 Hz  G
#define gs4 2408    // 415 Hz  A flat / G sharp
#define a4  2273    // 440 Hz  A
#define bb4 2145    // 466 Hz  B flat
#define b4  2025    // 493 Hz  B

#define c5  1911    // 523 Hz  C 
#define cs5 1804    // 554 Hz  D flat / C sharp 
#define d5  1703    // 587 Hz  D 
#define eb5 1607    // 622 Hz  D sharp / E flat
#define e5  1517    // 659 Hz  E
#define f5  1432    // 698 Hz  F
#define fs5 1351    // 740 Hz  G flat / F sharp
#define g5  1276    // 784 Hz  G
#define gs5 1204    // 831 Hz  A flat / G sharp
#define a5  1136    // 880 Hz  A
#define bb5 1073    // 932 Hz  B flat
#define b5  1012    // 988 Hz  B

#define c6  956     // 1047 Hz  C 
#define cs6 902     // 1109 Hz  D flat / C sharp 
#define d6  851     // 1175 Hz  D 
#define eb6 804     // 1245 Hz  D sharp / E flat
#define e6  758     // 1319 Hz  E
#define f6  716     // 1397 Hz  F
#define fs6 676     // 1480 Hz  G flat / F sharp
#define g6  638     // 1568 Hz  G
#define gs6 602     // 1661 Hz  A flat / G sharp
#define a6  568     // 1760 Hz  A
#define bb6 536     // 1865 Hz  B flat
#define b6  506     // 1976 Hz  B

#define c7  478     // 2093 Hz  C 
#define cs7 451     // 2217 Hz  D flat / C sharp 
#define d7  426     // 2349 Hz  D 
#define eb7 402     // 2489 Hz  D sharp / E flat
#define e7  379     // 2637 Hz  E
#define f7  358     // 2794 Hz  F
#define fs7 338     // 2960 Hz  G flat / F sharp
#define g7  319     // 3136 Hz  G
#define gs7 301     // 3322 Hz  A flat / G sharp
#define a7  284     // 3520 Hz  A
#define bb7 268     // 3729 Hz  B flat
#define b7  253     // 3951 Hz  B

#define R   000         // R is a rest, no tone is played

/*=============================================================================
* Now we define the Note durations (how long each not will play.
* If you wish to play a note of duration longer than these,
* or of some other multiple (like a dotted quarter note), just add
* them up.   Example: dotted quarter = q + e
*            Example: dotted eighth = e + s
==============================================================================*/

#define q 100  // Quarter Note
#define h 200  // Half Note
#define W 400  // Whole Note
#define e 50   // Eighth Note
#define s 25   // Sixteenth Note

/*=============================================================================
* Now we define the class of our library, that is Music
* Within this plac we have a public constructor, called Music and a public
* method called playMusic which returns nothing but plays the tune we enter to 
* it as an integer array
==============================================================================*/
class Music
{
    public:
        Music(int speaker);          //constructor, setsup up the speaker pin                                       
        void playMusic(int song[], int length);  //method which plays the music, song and its size
    private:
        int _speaker;                //private variable used in the Music class
};

#endif
