/* Music Library C++ file
 * Luke Spicer, March 17th, 2009
 * Use this Libary to play music/tones with
 * a piezo element and your Arduino!
 */

#include "Music.h"     //Include the header file Music.h so our library works

/*=============================================================================
* This constructor begins the music class and initializes the speaker pin
* The input to Music() is an integer corresponding to a digital I/0 pin on the
* arduino board. This constructor sets up that pin as OUTPUT and defines our
* private variable from Music.h, _speaker, equal to our chosen pin
==============================================================================*/
Music::Music(int speaker)       //Class::Constructor(Datatype variable)                                                 
{
    pinMode(speaker, OUTPUT);   //pinMode is an arduino standard function
    _speaker = speaker;
}
/*=============================================================================
* This is the fun part!!! Getting on off electrical pulses to sound like tones
* (and maybe even music if you wish). The general idea is that cycling On/Off
* the piezo element pin at the frequency we hear as a given note will produce
* that note. To do this the user will enter their song as an integer array. The
* first value in that array will be the BPM (beats per minute), to approximate
* tempo. After the BPM the following values will be alternating note names and
* and durations. Example: c4,q  (that's middle c held for a quarter note). The
* available note names and durations are listed in Music.h as #define constants.
*                                HAVE FUN!!!
==============================================================================*/
void Music::playMusic(int song[], int length)     //void means this method returns nothing                                            
{
  length /= sizeof(song[0]);         //use song array length and note size to get # of notes in song
  long tempo = 600000/song[0];       //tempo is determined by song[0] = BPM
  for(int i = 1; i < length; i += 2) //notes start at song[1]
  {
    int period = song[i];             //the period of our note from Music.h
    long duration = long(song[i+1]) * tempo; //actual duration, from tempo
    if(period == 0)                   //If the period is zero, it's a rest
    {
                               //If the note is a rest, play nothing
      delay(duration/1000);    //Play nothing for the duration requested
    }
    else                              //Else the note is not a rest, play it!
    {
      long t = 0;                     //t is how long the note has been played
      while(t <= duration)            //if t is less than duration, play on!
      {
        digitalWrite(_speaker, HIGH); //turn on piezo element
        delayMicroseconds(period/2);  //on for half of period
        digitalWrite(_speaker, LOW);  //turn off piezo element
        delayMicroseconds(period/2);  //off for half of period
        t += period;                  // t = t + period, play time so far
      }
      digitalWrite(_speaker, LOW);    //turn off the piezo after note completed
      delay(15);                      //the delay makes our notes discrete
    }
  }
}
