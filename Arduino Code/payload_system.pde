#include <EEPROM.h>
#include <Wire.h>                //arduino library for I2C comm (req'd for bmp, adc, and mux)
#include <BMP085.h>              //adafruit library for baro
#include <MUX.h>                 //custom library for I2C mux (PCA9544A)
#include <ADS7828.h>             //custom library for I2C ADC (ADS7828)
#include <WirelessComm.h>        //custom library for wireless comm
#include <GPS.h>                 // Custom GPS Parser Library
#include <VinMonitor.h>          // Custom Battery Voltage Monitor Library
#include <NewSoftSerial.h>     // Arduino Libary to allow for software UART interfaces
#include <IMU.h>                 //custom library for Sparkfun 5DOF IMU
#include <Adafruit_VC0706.h>
//#include <SD.h>

#define SERIALNUM 3              //Serial port used by myWireless
#define chipSelect 53
#define pictureSaveTime 500

// Using hardware serial on Mega: camera TX conn. to RX1,
// camera RX to TX1, no SoftwareSerial object is required:
Adafruit_VC0706 cam = Adafruit_VC0706(&Serial1);
int pictureState = 0; // 0 = take picture, 1 = save picture, 2 = transmit picture
uint16_t jpglen = 0;
unsigned long picFileSize = 0;
unsigned long picFilePos = 0;
char picname[13];
byte pic_number = 0;

NewSoftSerial softSerial(62, 63); //initialize softserial and file_name for UART
char file_name[10];
int fileNum = 0;

ADS7828 ads0(0);                 //initialize ADC

BMP085 bmp;                      //initialize baro
float presOffset = 101988;       //pressure at sea level (baro)

long arduino_time;            // Variable to store the global Arduinoarduino_timeach cycle of loop()

// GPS and Supporting Objects and Variables
GPS my_gps;                    // GPS object connected to GPS connector
int gps_latitude_degrees = 0;
int gps_latitude_minutes = 0;
float gps_latitude_seconds = 0.0;
int gps_longitude_degrees = 0;
int gps_longitude_minutes = 0;
float gps_longitude_seconds = 0.0;
int gps_time_hours = 0;
int gps_time_minutes = 0;
float gps_time_seconds = 0.0;
float gps_altitude;
float gps_speed;
float gps_course;
int isFixed = 0;               // isFixed = 1, GPS has valid fix data

//myWireless and supporting objects and vars
WirelessComm myWireless(SERIALNUM);


float gpsVars[9] = {84.023, 35.031, 22, 88.023, 30.031, 29, 102, 12, 7}; //latd latm lats lond lonm lons alt spd nsat
float barVars[3] = {128.023, 124, 23.4}; //pressure, altitude, temp
float humVars[1] = {55.2}; //relative humidity
float sirVars[1] = {60.01232}; //solar irradiance
float uvVars[1]  = {11.19988}; //UV irradiance
float rgbVars[3] = {10, 20, 30}; //red, blue, green
float imuVars[5] = {0.2, 0.7, 20, 0.1, 0.3}; //xa ya za xr yr

String gps = "GPS";
String bar = "BAR";
String hum = "HUM";
String sir = "SIR";
String uv  = "UV";
String rgb = "RGB";
String imu = "IMU";

// Vin Battery Voltage Monitor Object and Variables
int Vin_analog_input_pin = 0; // Vin (Battery) Voltage Monitor connected to analog pin 0
float Vref = 5.00;            // Vref measured value
float Rref = 42.80;           // Rref measured value
float Rup = 42.80;            // Rup measured value
float diode_drop = 0.84;      // Input protection diode voltage drop
VinMonitor battery(Vin_analog_input_pin, Vref, Rref, Rup, diode_drop);

//analog pins, and reference voltages and other supporting vars along with IMU object 
int xa = 3;
int ya = 4;
int za = 5;
int yrate = 6;
int xrate = 7;
//float Vref = 4.82;  //use Vref from VinMonitor
float V3_3 = 3.312;
IMU my_imu(xa, ya, za, yrate, xrate, V3_3, Vref);
float x_acceleration;
float y_acceleration;
float z_acceleration;
float y_rotation_rate;
float x_rotation_rate;

int bmpflag;

boolean scienceBoardExist[] = {1, 1, 1, 0, 0, 0, 0, 0};
boolean baro1Exist[] = {1, 1, 1, 0, 0, 0, 0, 0};
boolean baro2Exist[] = {1, 1, 1, 0, 0, 0, 0, 0};
boolean uvExist[] = {1, 1, 1, 0, 0, 0, 0, 0};
boolean sirExist[] = {1, 1, 1, 0, 0, 0, 0, 0};
boolean rgbExist[] = {1, 1, 1, 0, 0, 0, 0, 0};
boolean humExist[] = {1, 1, 1, 0, 0, 0, 0, 0};

void setup() 
{
  myWireless.beginSerial();      //initialize myWireless
  Serial2.begin(9600);           //initialize Serial2 (for GPS)
  Serial.begin(9600);            //initialize debug serial
  softSerial.begin(57600);
  
  sprintf(file_name,"log%d.txt", fileNum);
  appendFile(file_name);
  //header
  for(int i = 0; i <8; i++){
    if (scienceBoardExist[i]){
      softSerial.print("#");
      softSerial.print(i);
      softSerial.print(",");
      MUX mux0(i);
      if (humExist[i] || sirExist[i] || uvExist[i] || rgbExist[i]){
        mux0.open_chan(0);
        if (humExist[i]){;
          softSerial.print("Humidity");
          softSerial.print(",");
        }
        if (sirExist[i]){
          softSerial.print("Solar Irradiance");
          softSerial.print(",");
        }
        if (uvExist[i]){
          softSerial.print("Ultraviolet");
          softSerial.print(",");
        }
        if (rgbExist[i]){
          softSerial.print("RGB Sensor");
          softSerial.print(",");
        }
      }
      if (baro1Exist[i]){
        softSerial.print("Barometer");
        softSerial.print(",");
      }
    }
  }
  softSerial.print("Arduino Time");
  softSerial.print(", ");
  softSerial.print("GPS Hours");
  softSerial.print(":");
  softSerial.print("GPS Minutes");
  softSerial.print(":");
  softSerial.print("GPS Seconds");
  softSerial.print(", ");
  softSerial.print("Battery V in");
  softSerial.print(", ");
  softSerial.print("Latitude Degrees");
  softSerial.print(", ");
  softSerial.print("Latitude Minutes");
  softSerial.print(", ");
  softSerial.print("Latitude Seconds");
  softSerial.print(", ");
  softSerial.print("Longitude Degrees");
  softSerial.print(", ");
  softSerial.print("Longitude Minutes");
  softSerial.print(", ");
  softSerial.print("Longitude Seconds");
  softSerial.print(", ");
  softSerial.print("Altitude");
  softSerial.print(", ");
  softSerial.print("Course");
  softSerial.print(", ");
  softSerial.print("Speed");
  softSerial.print(", ");
  softSerial.print("X Acceleration");
  softSerial.print(", ");
  softSerial.print("Y Acceleration");
  softSerial.print(",");
  softSerial.print("Z Acceleration");
  softSerial.print(",");
  softSerial.print("X Rotation Rate");
  softSerial.print(", ");
  softSerial.print("Y rotation Rate");
  softSerial.print(", ");
  
  // see if the card is present and can be initialized:
//  if (!SD.begin(chipSelect)) {
//    Serial.println("Card failed, or not present");
//  }  
  
  // Try to locate the camera
  if (cam.begin()) {
    Serial.println("Camera Found:");
  } else {
    Serial.println("No camera found?");
  }
  // Print out the camera version information (optional)
  char *reply = cam.getVersion();
  if (reply == 0) {
    Serial.print("Failed to get version");
  } else {
    Serial.println("-----------------");
    Serial.print(reply);
    Serial.println("-----------------");
  }

  // Set the picture size - you can choose one of 640x480, 320x240 or 160x120 
  // Remember that bigger pictures take longer to transmit!
  
  //cam.setImageSize(VC0706_640x480);        // biggest
  cam.setImageSize(VC0706_320x240);        // medium
  //cam.setImageSize(VC0706_160x120);          // small

  // You can read the size back from the camera (optional, but maybe useful?)
  uint8_t imgsize = cam.getImageSize();
  Serial.print("Image size: ");
  if (imgsize == VC0706_640x480) Serial.println("640x480");
  if (imgsize == VC0706_320x240) Serial.println("320x240");
  if (imgsize == VC0706_160x120) Serial.println("160x120");
}

void loop()
{
  arduino_time = millis()/1000;
  // Call a custom funtion (which is defined below) that will read the GPS
  read_gps();
  appendFile(file_name);
  softSerial.println();
  //Read 8 Science Boards
  for(int i = 0; i <8; i++){
    if (scienceBoardExist[i]){
      softSerial.print("#");
      softSerial.print(i);
      softSerial.print(",");
      MUX mux0(i);
      if (humExist[i] || sirExist[i] || uvExist[i] || rgbExist[i]){
        mux0.open_chan(0);
        if (humExist[i]){
          humVars[0] = ads0.read_chan(0);
          softSerial.print(humVars[0]);
          softSerial.print(",");
          myWireless.transmitData(millis(), hum, i, humVars);
        }
        if (sirExist[i]){
          sirVars[0] = ads0.read_chan(2);
          softSerial.print(sirVars[0]);
          softSerial.print(",");
          myWireless.transmitData(millis(), sir, i, sirVars);
        }
        if (uvExist[i]){
          uvVars[0] = ads0.read_chan(3);
          softSerial.print(uvVars[0]);
          softSerial.print(",");
          myWireless.transmitData(millis(), uv, i, uvVars);
        }
        if (rgbExist[i]){
          rgbVars[0] = ads0.read_chan(4);
          rgbVars[1] = ads0.read_chan(5);
          rgbVars[2] = ads0.read_chan(6);
          softSerial.print(rgbVars[0]);
          softSerial.print(",");
          softSerial.print(rgbVars[1]);
          softSerial.print(",");
          softSerial.print(rgbVars[2]);
          softSerial.print(",");
          myWireless.transmitData(millis(), rgb, i,rgbVars);
        }
      }
      if (baro1Exist[i]){
        mux0.open_chan(1);
        bmp.begin();
        barVars[0] = bmp.readPressure();
        barVars[1] = bmp.readAltitude(presOffset);
        barVars[2] = bmp.readTemperature();
        softSerial.print(barVars[0]);
        softSerial.print(",");
        softSerial.print(barVars[1]);
        softSerial.print(",");
        softSerial.print(barVars[2]);
        softSerial.print(",");
        myWireless.transmitData(millis(), bar, i, barVars);
      }
    }
  }

  gpsVars[0] = gps_latitude_degrees;
  gpsVars[1] = gps_latitude_minutes;
  gpsVars[1] = gps_latitude_seconds;
  gpsVars[3] = gps_longitude_degrees;
  gpsVars[4] = gps_longitude_minutes;
  gpsVars[5] = gps_longitude_seconds;
  gpsVars[6] = gps_altitude;
  gpsVars[7] = gps_speed;
  gpsVars[8] = 0;
  
  /*************Ask Luke about NumSat******************/
  
  x_acceleration = my_imu.get_x_acc();
  y_acceleration = my_imu.get_y_acc();
  z_acceleration = my_imu.get_z_acc();
  y_rotation_rate = my_imu.get_y_rate();
  x_rotation_rate = my_imu.get_x_rate();
  imuVars[0] = x_acceleration;
  imuVars[1] = y_acceleration;
  imuVars[2] = z_acceleration;
  imuVars[3] = x_rotation_rate;
  imuVars[4] = y_rotation_rate;
  
  myWireless.transmitData(millis(), gps, 0, gpsVars);
  myWireless.transmitData(millis(), imu, 0, imuVars);

  Serial.print("Time = ");
  Serial.print(arduino_time);
  Serial.println(" sec");
  softSerial.print(arduino_time);
  softSerial.print(", ");
  softSerial.print(gps_time_hours);
  softSerial.print(":");
  softSerial.print(gps_time_minutes);
  softSerial.print(":");
  softSerial.print(gps_time_seconds);
  softSerial.print(", ");
  softSerial.print(battery.get_Vin());
  softSerial.print(", ");
  softSerial.print(gps_latitude_degrees);
  softSerial.print(", ");
  softSerial.print(gps_latitude_minutes);
  softSerial.print(", ");
  softSerial.print(gps_latitude_seconds,5);
  softSerial.print(", ");
  softSerial.print(gps_longitude_degrees);
  softSerial.print(", ");
  softSerial.print(gps_longitude_minutes);
  softSerial.print(", ");
  softSerial.print(gps_longitude_seconds,5);
  softSerial.print(", ");
  softSerial.print(gps_altitude);
  softSerial.print(", ");
  softSerial.print(gps_course);
  softSerial.print(", ");
  softSerial.print(gps_speed);
  softSerial.print(", ");
  softSerial.print(x_acceleration);
  softSerial.print(", ");
  softSerial.print(y_acceleration);
  softSerial.print(",");
  softSerial.print(z_acceleration);
  softSerial.print(",");
  softSerial.print(x_rotation_rate);
  softSerial.print(", ");
  softSerial.print(y_rotation_rate);
  softSerial.print(", ");
  Serial.println("...done writing to log file");

  if(pictureState == 0) {
    Serial.println("Snap in 1 secs...");
    delay(1000);
  
    if (! cam.takePicture()) 
      Serial.println("Failed to snap!");
    else 
      Serial.println("Picture taken!");
    
    // Create an image with the name IMAGExx.JPG
    strcpy(picname, "IMAGE000.JPG");
    byte picNum = EEPROM.read(30);
    pic_number = picNum;
    picname[5] = '0' + picNum/100;
    picname[6] = '0' + (picNum%100)/10;
    picname[7] = '0' + picNum%10;
    
    // Get the size of the image (frame) taken  
    jpglen = cam.frameLength();
    picFileSize = jpglen;
    Serial.print("Picture is ");
    Serial.print(jpglen, DEC);
    Serial.println(" byte image.");
    
    pictureState = 1;
  }
  
	if(pictureState == 1) {
    // Open the file for writing
    appendFile(picname);
    
    // Read all the data up to # bytes!
    byte wCount = 0; // For counting # of writes
    long timer = millis();

    while ((millis() - timer) < pictureSaveTime) {
      // read 32 bytes at a time;
      uint8_t *buffer;
      uint8_t bytesToRead = min(64, jpglen); // change 32 to 64 for a speedup but may not work with all setups!
      buffer = cam.readPicture(bytesToRead);
	  
	  // Write image data to disk
      for(int n = 0; n < bytesToRead; n++) {
        softSerial.print(buffer[n]);
      }
	  
	  // Send state of buffer as packet of image data
	  myWireless.transmitImage(millis(), pic_number, buffer, bytesToRead);
	  
      jpglen -= bytesToRead;
    }
  
    if(jpglen <= 0) { 
      Serial.print(picname);
      Serial.println(" finished saving!");
      pic_number++;
      EEPROM.write(30,int(pic_number));
      cam.resumeVideo();
      pictureState = 0;
      jpglen = 0;
      picFilePos = 0;
      Serial.print("Number of bytes in file = ");
      Serial.println(picFileSize);
    }
  }
}

//
// Function: read_gps()
//
// Input: void
// Return: void
//
// Description: Use this function to read your GPS device for a NMEA message using
//              read_gps_line(), check it for a valid checksum, and display with
//              full verbosity the conents of the message input. 
//              The while((millis() - start_time)<500) will read the GPS for 500mSec
//              before continuing the loop, this time can be set as low as you want and is simply
//              used to give the Arduino time to read the GPS, especially if the remainting parts 
//              of the loop() take a lot of time.
//
void read_gps() {
   // Save the time the the read_gps() function began
  long start_time = millis();
  
  // Keep reading/updating the GPS for up to 500mSec
  while((millis() - start_time) < 500) {
    // read_gps_line() reads Serial2 and take in a NMEA string
    // if it returns something other than 1, it did not get a full message
    int test = my_gps.read_gps_line();
    if(test == 1) {
      // We know we got a full message so we use get_gps_line() to return the
      // NMEA message for disply on the serial monitor
      String test_message = my_gps.get_gps_line();
   
      // Next we use par_NMEA_message to get the data from the message and validate
      // its checksum. If this function call returns 0 the checksum was invalid, so don't
      // bother getting the data from it.
      // By selecting VERBOSE mode, the parse_NMEA_message displays on the serial monitor
      // all the fields it just parsed. If you leave this input empty, it won't do that.
      int test_valid = my_gps.parse_NMEA_message(test_message);
      if(test_valid == 1) {
        Serial.println("Checksum Valid");
        
        /***** Update GPS Data Objects *****/
        Latitude test_latitude = Latitude(my_gps.get_latitude_string());
        gps_latitude_degrees = test_latitude.get_degrees();
        gps_latitude_minutes = test_latitude.get_minutes();
        gps_latitude_seconds = test_latitude.get_seconds();

        
        Longitude test_longitude = Longitude(my_gps.get_longitude_string());
        gps_longitude_degrees = test_longitude.get_degrees();
        gps_longitude_minutes = test_longitude.get_minutes();
        gps_longitude_seconds = test_longitude.get_seconds();
        
        Time test_time = Time(my_gps.get_time_string());
        gps_time_hours = test_time.get_hours();
        gps_time_minutes = test_time.get_minutes();
        gps_time_seconds = test_time.get_seconds();
        
        gps_altitude = my_gps.get_altitude();
        
        gps_course = my_gps.get_course();
        
        gps_speed = my_gps.get_speed(MPH);
        /*****                         *****/
        
        // Test Status Indicators
        if(my_gps.get_data_status() == 0) {
          Serial.println("Data is invalid!");
          isFixed = 0;
        }
        else if(my_gps.get_data_status() == 1) {
          Serial.println("Data is valid");
          isFixed = 1;
        }
        else {
          Serial.println("Data status unknown");
          isFixed = 0;
        }
        
        if(my_gps.get_fix_mode() == 1) {
          Serial.println("No Fix Available");
          isFixed = 0;
        }
        else if(my_gps.get_fix_mode() == 2) {
          Serial.println("2D Fix Available");
          isFixed = 1;
        }
        else if(my_gps.get_fix_mode() == 3){
          Serial.println("3D Fix Available");
          isFixed = 1;
        }
        else {
          Serial.println("Fix Mode unknown");
          isFixed = 0;
        }
        
        if(my_gps.get_fix_status() == 0) {
          Serial.println("No Fix Available");
          isFixed = 0;
        }
        else if(my_gps.get_fix_status() == 1) {
          Serial.println("Standard GPS Fix Available");
          isFixed = 1;
        }
        else {
          Serial.println("Fix Status Unknown");
          isFixed = 0;
        }
        
        if(my_gps.get_mode_ind() == 0) {
          Serial.println("Mode Indicates 'No Fix'");
          isFixed = 0;
        }
        else {
          Serial.println("Mode Indicates a fix");
          isFixed = 1;
        }
        Serial.println();
      }
      // If parse_NMEA_message() returns -1 the message was not of valid type
      else if(test_valid == -1) {
        Serial.println("***** Message was not of type GGA, GSA, RMC or VTG *****\n");
      }
      // If parse_NMEA_message() returns 0 the message checksum was invalid
      else {
        Serial.println("Checksum Invalid\n");
      }
    }
    // If read_gps_line() returns -1 it means before a full message
    // was read in the watchdog timmed out. This is useful, so that
    // a broken or malfunctioning GPS wan't lockup the entire system
    else if (test == -1) {
      Serial.println("***** Got incomplete message and timed out *****\n");
    }
  }
}

void appendFile(char fileName[]){
  //Send three control z to enter OpenLog command mode
  softSerial.print(byte(26));
  softSerial.print(byte(26));
  softSerial.print(byte(26));

  //Wait for OpenLog to respond with '>' to indicate we are in command mode
  long timer = millis();
  while((millis() - timer) < 150) {
    if(softSerial.available()) {
      if (softSerial.read() == '>') break;
    }
  }

  String file_name = String(fileName);
  file_name = String("append ") + fileName + String("\r");
  softSerial.print(file_name);
  Serial.println(file_name);

  //Wait for OpenLog to indicate file is open and ready for writing
  timer = millis();
  while((millis() - timer) < 150) {
    if(softSerial.available()) {
      if(softSerial.read() == '<') break;
    }
  }
}
