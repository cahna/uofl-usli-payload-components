/*
 *  University of Louisville USLI, 2011-2012
 *  Filename		: ADS7828.cpp
 *	Written by		: Nicholas Searcy [Luke Spicer, Nathan Armentrout]
 *  Date			: 14 December 2011
 *  Description		: Library used to control up to 4 ADS7828 Analog to digital 
 *  	Converters (ADCs) on a single I2C line. The constructor should be used for each ADC. Each 
 *  	ADC has 8 channels. read_chan(byte channel) is used to read a channel (0 - 7).
 *  [Dependencies	: (Wire.h --standard Arduino Library)]
 */

/*
 * TODO: make backward compatible with legacy Arduino IDE, specifically, the Wire.h commands
 * 		such as Wire.write() and Wire.read(). BMP085 library is a great example of how to do this
 */

#include "ADS7828.h"


ADS7828::ADS7828(byte address)
{
	_address = address;
	Wire.begin();
	//see notes below for address byte description
	_ADS_BASE_ADDRESS = 0x48;
};

int ADS7828::read_chan(byte channel)
{
	_channel = channel;
	//See notes below for Lookup Table
	switch(_channel){
		case 0:
		  _channel = B10000100;
		  break;
		case 1:
		  _channel = B11000100;
		  break;
		case 2:
		  _channel = B10010100;
		  break;
		case 3:
		  _channel = B11010100;
		  break;
		case 4:
		  _channel = B10100100;
		  break;
		case 5:
		  _channel = B11100100;
		  break;
		case 6:
		  _channel = B10110100;
		  break;
		case 7:
		  _channel = B11110100;
		  break;
		default:
		  _channel = B10000100;
		  break;
	}//switch
  
  	//Send command byte
  	Wire.beginTransmission(_ADS_BASE_ADDRESS | _address);
  	Wire.write(_channel);
  	Wire.endTransmission();
  
  	// Receieve ADC value
  	Wire.beginTransmission(_ADS_BASE_ADDRESS | _address);
  	Wire.requestFrom(_ADS_BASE_ADDRESS | _address,2);
  	Wire.endTransmission();
  
  	//Convert byte pair to int
  	if (Wire.available()){
    _value += Wire.read() << 8;
    	if (Wire.available()){
      		_value += Wire.read();
   	 	}else{
      		return - 1;
    	}//else
  	}else{
    	return -2;
  	}//else
  
  	return _value;
  	
};//read_chan

/* TI Byte notes
Address byte: 10010[A1][A0][R/W']
R/W' is supported by Wire, therefore, use only 7 bit addresses

Command byte: [SD][C2][C1][C0][PD1][PD0][X][X]

Value([C2][C1][C0])  Channel
0                    0
1                    2
2                    4
3                    6
4                    1
5                    3
6                    5
7                    7

Lookup table created in readADC() given strange correlation
*/
