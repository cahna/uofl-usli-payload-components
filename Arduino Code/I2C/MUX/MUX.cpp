/*
 *  University of Louisville USLI, 2011-2012
 *  Filename		: MUX.cpp
 *	Written by		: Nicholas Searcy [Luke Spicer, Nathan Armentrout]
 *  Date			: 14 December 2011
 *  Description		: Library used to control up to 8 PCA9544 Multiplexers (MUXs)
 *  	on a single I2C line. The constructor should be used for each MUX. Each 
 *  	MUX has 4 channels and only one channel may be open at a time. 
 *  	open_chan(byte channel) is used to open a channel (0 - 3), it also closes 
 *  	the other 3 channels. close_chan() closes all four channels.
 *  [Dependencies	: (Wire.h --standard Arduino Library)]
 */

/*
 * TODO: make backward compatible with legacy Arduino IDE, specifically, the Wire.h commands
 * 		such as Wire.write() and Wire.read(). BMP085 library is a great example of how to do this
 */

#include "MUX.h"


MUX::MUX(byte address)
{
	_address = address;
	Wire.begin();
	_MUX_BASE_ADDRESS = 0x70;
	_MUX_BASE_CONTROL = 0x04;
	_MUX_CLOSE_CONTROL = 0x00;
};

void MUX::open_chan(byte channel)
{
	_channel = channel;
	Wire.beginTransmission(_MUX_BASE_ADDRESS | _address);
  	Wire.write(_MUX_BASE_CONTROL | _channel);
  	Wire.endTransmission();
};

void MUX::close_chan()
{
	Wire.beginTransmission(_MUX_BASE_ADDRESS | _address);
	byte zero = 0x00;
  	Wire.write(zero);
  	Wire.endTransmission();
};

/*
 * MUX_BASE_ADDRESS:
 * [1][1][1][0][A2][A1][A0][R/W']
 * Becase Wire.h takes care of R/W' (thanks Nathan) we should only
 * include the first 7 bits
 * 
 * MUX_BASE_CONTROL:
 * [INT3][INT2][INT1][INT0][x][B2][B1][B0]
 * INT bits are READ ONLY, x is Don't Care, B2 is the enable bit and B1B0 are
 * select bits
 */
 */
