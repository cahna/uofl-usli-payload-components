/*
 *  University of Louisville USLI, 2011-2012
 *  Filename		: MUX.cpp
 *	Written by		: Nicholas Searcy [Luke Spicer, Nathan Armentrout]
 *  Date			: 14 December 2011
 *  Description		: Library used to control up to 8 PCA9544 Multiplexers (MUXs)
 *  	on a single I2C line. The constructor should be used for each MUX. Each 
 *  	MUX has 4 channels and only one channel may be open at a time. 
 *  	open_chan(byte channel) is used to open a channel (0 - 3), it also closes 
 *  	the other 3 channels. close_chan() closes all four channels.
 *  [Dependencies	: (Wire.h --standard Arduino Library)]
 */

/*
 * TODO: make backward compatible with legacy Arduino IDE, specifically, the Wire.h commands
 * 		such as Wire.write() and Wire.read(). BMP085 library is a great example of how to do this
 */

#ifndef MUX_h
#define MUX_h

#if (ARDUINO >= 100)
	#include "Arduino.h"
#else
	#include "WProgram.h"
#endif
#include "Wire.h"

#define MUX_DEBUG 1 	

class MUX
{
	public:
		MUX(byte address);
		void open_chan(byte channel);
		void close_chan();
	private:
		byte _address;
		byte _channel;
		byte _MUX_BASE_ADDRESS;
		byte _MUX_BASE_CONTROL;
		byte _MUX_CLOSE_CONTROL;
};

#endif