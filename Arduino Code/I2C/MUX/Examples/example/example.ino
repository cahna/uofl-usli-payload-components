#include <Wire.h>
#include <MUX.h>

MUX mux0(0);


void setup(){
  Serial.begin(9600);
  
};

void loop(){
  mux0.open_chan(0);
  flash(500);
  mux0.open_chan(1);
  flash(500);
  mux0.open_chan(2);
  flash(500);
  mux0.open_chan(3);
  flash(500);
  mux0.close_chan();
  flash(500);
  Serial.println("hola");
};

void flash(int time){
  int t = millis();
  byte zero = 0x00;
  while (millis()-t < time){
    Wire.beginTransmission(zero);
    Wire.write(zero);
    Wire.endTransmission();
    delay(0);
  };
  
};
