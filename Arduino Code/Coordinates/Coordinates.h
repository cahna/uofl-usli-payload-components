//
//  point.h
//  UofLUSLI
//
//  Created by Nicholas Searcy on 1/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef COORDINATES_H
#define COORDINATES_H

#include <Vector.h>
#include <math.h>

class Coordinates {
    
public:
    Coordinates();
    Coordinates(int lat_degrees, int lat_minutes, double lat_seconds, int lon_degrees, int lon_minutes, double lon_seconds);
    void map_point(int lat_minutes, double lat_seconds, int lon_minutes, double lon_seconds);
    float map_COG(float angle);
    double getX();
    double getY();
    double getX0();
    double getY0();
    double getSqRatio();
    
private:
    double x;
    double y;
    double x0;
    double y0;
    double sqRatio;
};

#endif
