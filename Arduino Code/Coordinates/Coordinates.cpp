//
//  point.cpp
//  UofLUSLI
//
//  Created by Nicholas Searcy on 1/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#include "Coordinates.h"

#define PI 3.14159265

Coordinates::Coordinates(){
    
}

Coordinates::Coordinates(int lat_degrees, int lat_minutes, double lat_seconds, int lon_degrees, int lon_minutes, double lon_seconds){
    lat_seconds += 60*lat_minutes;
    lon_seconds += 60*lon_minutes;
    y0 = lat_seconds;
    x0 = lon_seconds;
    sqRatio = cos((lat_degrees+(lat_minutes/60))*PI/180);
}

void Coordinates::map_point(int lat_minutes, double lat_seconds, int lon_minutes, double lon_seconds){
    lat_seconds += 60*lat_minutes;
    lon_seconds += 60*lon_minutes;
    x = lon_seconds - x0;
    y = lat_seconds - y0;
    
    if (x < -1800){
        x += 3600;
    }
    else if (x > 1800){
        x -= 3600;
    }
    if (y < -1800) {
        y += 3600;
    }
    else if (y > 1800){
        y -= 3600;
    }
	x = x/sqRatio;
}

float Coordinates::map_COG(float angle){
	Vector COG = Vector(90-angle);
	double x1 = COG.getX()/sqRatio;
	double y1 = COG.getY();
	COG.setXY(x1,y1);
	return COG.getAngle();
	
}

double Coordinates::getX(){
    return x;
}

double Coordinates::getY(){
    return y;
}

double Coordinates::getX0() {
    return x0;
}

double Coordinates::getY0() {
    return y0;
}

double Coordinates::getSqRatio()
{
    return sqRatio;
}