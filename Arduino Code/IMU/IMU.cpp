//
// University of Louisville USLI, 2011-2012
//
// Filename		:	IMU.cpp
// Written by	:	Lucas Spicer
// Date			:	13 November 2011
// Description 	:	C++ file which contains function definitions for the custom
//					IMU class. IMU constructor connects the IMU to specified Arduino
//					Analog pins and sets the reference voltage. Functions are also provided
//					to zero out the device an all the inputs, to return the A/D values
//					of all variables and zeroed values. And also to return as float point values
//					the acceleration in all 3 axes (in gs) as well as the rotational velocity in
//					by the x and y axes (in degrees/second)
//

#include "IMU.h"

//******* PUBLIC *******//

//
// IMU class constructor IMU()
//
// Input: 	int x_acc (the Arduino analog pin number the x_acc pin on the IMU is connected to)
//			int y_acc (the Arduino analog pin number the y_acc pin on the IMU is connected to) 
//			int z_acc (the Arduino analog pin number the z_acc pin on the IMU is connected to) 
//			int y_rate (the Arduino analog pin number the y_rate pin on the IMU is connected to) 
//			int x_rate (the Arduino analog pin number the x_rate pin on the IMU is connected to)
//			float V3_3 (the actual measured voltage of the 3.3V rail the IMU is powered by)
//			float Vref (the actual measured voltage of the 5V reference the Arduino is powered by)  
// Return: 	creates IMU Object and sets the 3.3V and 5V reference voltages and connects the IMU to the chosen
//			Arduino analog pins
//
// Description: Use this function to create a new IMU object (based on the Sparkfun 5DOF IMU)
//				Simpy specify which analog pins the IMU is connected to, and the exact values of the
//				3.3V supply and the	5V reference (if not specified the constructor defaults are
//				3.3 and 5.0V)
//
IMU::IMU(int x_acc, int y_acc, int z_acc, int y_rate, int x_rate,float V3_3, float Vref) {
  _x_acc_pin 	= x_acc;
  _y_acc_pin 	= y_acc;
  _z_acc_pin	= z_acc;
  _y_rate_pin	= y_rate;
  _x_rate_pin 	= x_rate;
  _Vref 		= Vref;
  _V3_3			= V3_3;
  _acc_ratio = _V3_3 * 100.0;		// The ratiometric g/V is based on the 3.3 supply voltage
  _zero_g = _V3_3 / 2.0;			// The zero g volage is the V3.3/2
}

// IMU "get_variable_AD" functions with the same form
//
// Function: get_"variable name"_AD()
//
// Input: 	void
// Return: 	integer value of the A/D conversion (Arduino analogRead() function) of the specified
//			variable
//
// Description: Use this function to get the analogRead() value of the specified variable
//
int	IMU::get_x_acc_AD() {
  return analogRead(_x_acc_pin);	// return A/D value (0 to 1023) of the x_acc value
}
int	IMU::get_y_acc_AD(){
  return analogRead(_y_acc_pin);	// return A/D value (0 to 1023) of the y_acc value
}
int	IMU::get_z_acc_AD(){
  return analogRead(_z_acc_pin);	// return A/D value (0 to 1023) of the z_acc value
}
int	IMU::get_y_rate_AD(){
  return analogRead(_y_rate_pin);	// return A/D value (0 to 1023) of the y_rate value
}
int	IMU::get_x_rate_AD(){
  return analogRead(_x_rate_pin);	// return A/D value (0 to 1023) of the x_rate value
}

//
// Function: zero()
//
// Input: 	int reading (integer number of readings to average zero values for) 
//			The function defaults to 10 readings if no value is specified
// Return: 	void
//
// Description: Use this function to zero the inputs of the IMU
//
void IMU::zero(int readings) {
  if (readings < 1) {
	readings = 1;					// Ensure we don't pass a value that's too small
  }
  if (readings > 60) {
	readings = 60;					// Ensure we don't pass a value that's too big (would cause overflow)
  }
  
  _x_acc_zero = 0;					// Initialize all zero values to zero before calculating the average
  _y_acc_zero = 0;
  _z_acc_zero = 0;
  _y_rate_zero = 0;
  _x_rate_zero = 0;
  
  // Take the number of readings specified by readings
  for(int i = 0; i < readings; i++) {
	_x_acc_zero += analogRead(_x_acc_pin);	// Calculate sum of all the readings of x_acc_AD
	_y_acc_zero += analogRead(_y_acc_pin);	// Calculate sum of all the readings of y_acc_AD
	_z_acc_zero += analogRead(_z_acc_pin);	// Calculate sum of all the readings of z_acc_AD
	_y_rate_zero += analogRead(_y_rate_pin);	// Calculate sum of all the readings of y_rate_AD
	_x_rate_zero += analogRead(_x_rate_pin);	// Calculate sum of all the readings of x_rate_AD
  }
  _x_acc_zero = _x_acc_zero/readings;		// Calculate the avarage of the readings, this is zero value
  _y_acc_zero = _y_acc_zero/readings;		// Calculate the avarage of the readings, this is zero value
  _z_acc_zero = _z_acc_zero/readings;		// Calculate the avarage of the readings, this is zero value
  _y_rate_zero = _x_rate_zero/readings;		// Calculate the avarage of the readings, this is zero value
  _x_rate_zero = _x_rate_zero/readings;		// Calculate the avarage of the readings, this is zero value
  
  _ground_axis = 3;							// Default axis toward earth is z-axis = 3;
  // If the x_acc_zero or y_acc_zero is lower than z_acc_zero, the ground axis is 1 or 2
  if ((_x_acc_zero < _z_acc_zero) || (_y_acc_zero < _z_acc_zero)) {
	_ground_axis = 2;
	if (_x_acc_zero < _y_acc_zero) {		// If the x_acc_zero  is lower than z_acc_zero the ground axis is 1
	  _ground_axis = 1;
	}
  }
}

// IMU "get_variable_zero" functions with the same form
//
// Function: get_"variable name"_zero()
//
// Input: 	void
// Return: 	integer averaged zeroed value of the specified variable 
//
// Description: Use this function to get the zeroed value of the specified variable
//
int IMU::get_x_acc_zero() {
  return _x_acc_zero;					// return the zeroed average A/D value of x_acc
}
int IMU::get_y_acc_zero() {
  return _y_acc_zero;					// return the zeroed average A/D value of y_acc
}
int IMU::get_z_acc_zero() {
  return _z_acc_zero;					// return the zeroed average A/D value of z_acc
}
int IMU::get_y_rate_zero() {
  return _y_rate_zero;					// return the zeroed average A/D value of y_rate
}
int IMU::get_x_rate_zero() {
  return _x_rate_zero;					// return the zeroed average A/D value of x_rate
}

//
// Function: get_y_rate()
//
// Input: 	void
// Return: 	float value of rotational velocity about y_axis in degrees/sec 
//
// Description: Use this function to get the y_axis rotational velocity
//
float IMU::get_y_rate() {
  float temp_value = 0;					// Temporary float value to store calculations
  temp_value = analogRead(_y_rate_pin) - _y_rate_zero;	// Calculate A/D offset from zeroed value
  temp_value = (temp_value * _Vref)/1024.0;				// Convert A/D offset to voltage offset
  temp_value = temp_value * 500.0;		// Degrees/Sec = voltage offset / 0.002 [V/degrees/sec]
  return temp_value;					// Return the rotational rate in degrees per second
}

//
// Function: get_x_rate()
//
// Input: 	void
// Return: 	float value of rotational velocity about x_axis in degrees/sec 
//
// Description: Use this function to get the x_axis rotational velocity
//
float IMU::get_x_rate() {
  float temp_value = 0;					// Temporary float value to store calculations
  temp_value = analogRead(_x_rate_pin) - _x_rate_zero;	// Calculate A/D offset from zeroed value
  temp_value = (temp_value * _Vref)/1024.0;				// Convert A/D offset to voltage offset
  temp_value = temp_value * 500.0;		// Degrees/Sec = voltage offset / 0.002 [V/degrees/sec]
  return temp_value;					// Return the rotational rate in degrees per second
}

//
// Function: get_x_acc()
//
// Input: 	void
// Return: 	float value of the acceleration in gs 
//
// Description: Use this function to get the x_axis acceleration
//
float IMU::get_x_acc() {
  float temp_value = 0;									// Temporary float value to store calculations
  temp_value = analogRead(_x_acc_pin) - _x_acc_zero;	// Read x_acc pin to get A/D value of x_acc
  temp_value = (temp_value * _Vref)/1024;				// Convert A/D value to a voltage
  //temp_value = temp_value - _zero_g;					// Calculate readings offset from the zero g volage level
  temp_value = (temp_value * 1000)/_acc_ratio; 			// Convert voltage offset to acc in g based on ratiometric value
  if(_ground_axis == 1) {								// I ground axis == 1, then account for x-axis being zeroed toward earth
	temp_value -= 1.0;
  }
  return temp_value;									// Return the acceleration in the x axis in "g"
}

//
// Function: get_y_acc()
//
// Input: 	void
// Return: 	float value of the acceleration in gs 
//
// Description: Use this function to get the y_axis acceleration
//
float IMU::get_y_acc() {
  float temp_value = 0;									// Temporary float value to store calculations
  temp_value = analogRead(_y_acc_pin) - _y_acc_zero;	// Read y_acc pin to get A/D value of y_acc
  temp_value = (temp_value * _Vref)/1024;				// Convert A/D value to a voltage
  //temp_value = temp_value - _zero_g;					// Calculate readings offset from the zero g volage level
  temp_value = (temp_value * 1000)/_acc_ratio; 			// Convert voltage offset to acc in g based on ratiometric value
  if(_ground_axis == 2) {								// I ground axis == 2, then account for y-axis being zeroed toward earth
	temp_value -= 1.0;
  }
  return temp_value;									// Return the acceleration in the y axis in "g"
}

//
// Function: get_z_acc()
//
// Input: 	void
// Return: 	float value of the acceleration in gs 
//
// Description: Use this function to get the z_axis acceleration
//
float IMU::get_z_acc() {
  float temp_value = 0;									// Temporary float value to store calculations
  temp_value = analogRead(_z_acc_pin) - _z_acc_zero;	// Read z_acc pin to get A/D value of z_acc
  temp_value = (temp_value * _Vref)/1024;				// Convert A/D value to a voltage
  //temp_value = temp_value - _zero_g;					// Calculate readings offset from the zero g volage level
  temp_value = (temp_value * 1000)/_acc_ratio; 			// Convert voltage offset to acc in g based on ratiometric value
  if(_ground_axis == 3) {								// I ground axis == 3, then account for z-axis being zeroed toward earth
	temp_value -= 1.0;
  }
  return temp_value;									// Return the acceleration in the z axis in "g"
}