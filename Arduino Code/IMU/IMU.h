//
// University of Louisville USLI, 2011-2012
//
// Filename		:	IMU.h
// Written by	:	Lucas Spicer
// Date			:	13 November 2011
// Description 	:	C++ Header file which contains a class defintion for a custom
//					IMU class (which is designed for use with the Sparkfun 5DOF freedom
//					IMU board).
//

#ifndef imu_h
#define imu_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

class IMU
{
	public:
		// IMU object Constructor
		IMU(int x_acc, int y_acc, int z_acc, int y_rate, int x_rate, float V3_3 = 3.3, float Vref = 5.0);
		// PUBLIC FUNCTIONS
		float get_Vref(void);
		void set_Vref(float Vref);
		int	get_x_acc_AD();
		int	get_y_acc_AD();
		int	get_z_acc_AD();
		int	get_y_rate_AD();
		int	get_x_rate_AD();
		
		void zero(int readings = 10);
		int get_x_acc_zero();
		int get_y_acc_zero();
		int get_z_acc_zero();
		int get_y_rate_zero();
		int get_x_rate_zero();
		
		float get_y_rate();
		float get_x_rate();
		float get_x_acc();
		float get_y_acc();
		float get_z_acc();
	private:
		// PRIVATE VARIABLES
		int _x_acc_pin;
		int _y_acc_pin;
		int _z_acc_pin;
		int _y_rate_pin;
		int _x_rate_pin;
		
		int _x_acc_zero;
		int _y_acc_zero;
		int _z_acc_zero;
		int _y_rate_zero;
		int _x_rate_zero;
		
		float _Vref;
		float _V3_3;
		float _acc_ratio;
		float _zero_g;
		int	_ground_axis;
};

#endif