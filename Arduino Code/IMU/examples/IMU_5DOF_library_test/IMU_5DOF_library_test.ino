//
//  University of Louisville USLI, 2011-2012
//
//  Filename    :  IMU_FDOF_library_test
//  Written by  :  Lucas Spicer
//  Date        :  13 November 2011
//  Description :  Arduino Example Sketch of the IMU library
//                 This library is written for use with the Sparkfun 5DOF IMU
//                 which consists of a ADXL335 Acceleromter and IDG-500 Gyro
//                 For details on the 5DOF IMU or the for the datasheets see
//                 http://www.sparkfun.com/products/9268
//                 For details on the funtions or constructors used in this example
//                 see the library files IMU.h and IMU.cpp.
//                 For use with a 3.3V Arduino set Vref = 3.3 supply and set V3_3 to = 3.3 supply
//

// Include the Library we wish to use in this example
#include <IMU.h>

// Create Variables to hold the values of the analog pins we wish to connect the IMU to
// It is not necessary to create variables to hold these values, you could just pass
// the values themselves to the constructor, but this method is used in the example for illustrative
// purposes
int xa = 3;
int ya = 4;
int za = 5;
int yrate = 6;
int xrate = 7;

// Vref is the measured voltage level that is the reference voltage (logic level) for your system,
// for a standard Arduino or Arduino mega this is about 5.0 Volts, on other devices it may be about
// 3.3 Volts. V3_3 is the seperate 3.3V power supply used to power the IMU if you're using a 5V Arduino.
// By creating the IMU object with the exact voltages for your system the accuracy is increased.
// If nothing is passed to the constructor for these values, the defaults are V3_3 = 3.3V and Vref = 5.0V
float Vref = 4.82;
float V3_3 = 3.312;

// Create an instance of our IMU class (name of this object is my_imu, but could be anything)
// Multiple instances of the IMU device can be created in one sketch, just name them different things
// and connect them to different pins
IMU my_imu(xa, ya, za, yrate, xrate, V3_3, Vref);

// Create integer variables to hold the A/D values for each measurement
int x_acc;
int y_acc;
int z_acc;
int y_rate;
int x_rate;

// Create float variables to hold the actual calcualted values for each measurment
float x_acceleration;
float y_acceleration;
float z_acceleration;
float y_rotation_rate;
float x_rotation_rate;

// All Arduino Sketches require both a setup() and loop() funtions, this is the setup()
void setup() {
  // Initialize the Serial port (the one connected to the USB interface) at 9600 for debugging and
  // receiving message on the computer's serial monitor
  Serial.begin(9600);
  
  // A friendly message to begin our test!
  Serial.println("***** IMU Library Test *****");
  
  // Zeroing the IMU is essentially the process of taking a number of measurments with the device in
  // a known inertial state, like sitting still on a flat surface and considering the average
  // of those measurements the starting point (the zero) against which to report all future measurements
  Serial.println("Zero IMU...");
  // We choose to take the average of 20 measurments as the "zero", but we could do 1 to 60
  my_imu.zero(20);
  
  // Print on the Serial Monitor the zero values (as A/D from 0 to 1023)
  Serial.print("X Acceleration Zero Value = ");
  Serial.println(my_imu.get_x_acc_zero());
  Serial.print("Y Acceleration Zero Value = ");
  Serial.println(my_imu.get_y_acc_zero());
  Serial.print("Z Acceleration Zero Value = ");
  Serial.println(my_imu.get_z_acc_zero());
  Serial.print("Y Rotation Rate Zero Value = ");
  Serial.println(my_imu.get_y_rate_zero());
  Serial.print("X Rotation Rate Zero Value = ");
  Serial.println(my_imu.get_x_rate_zero());
  Serial.println();
}

// All Arduino Sketches require both a setup() and loop() funtions, this is the loop()
void loop() {
  // At the start of each loop read the IMU outputs and print the A/D values on the Serial Monitor
  x_acc = my_imu.get_x_acc_AD();
  y_acc = my_imu.get_y_acc_AD();
  z_acc = my_imu.get_z_acc_AD();
  y_rate = my_imu.get_y_rate_AD();
  x_rate = my_imu.get_x_rate_AD();
  
  
  // Then read the sensors again to get the calculated values in g (for accerlation) or
  // degrees/second for the rotational velocity outputs. For the details of how these
  // values are calculated/returned, see IMU.cpp and the sensor datasheets
  x_acceleration = my_imu.get_x_acc();
  y_acceleration = my_imu.get_y_acc();
  z_acceleration = my_imu.get_z_acc();
  y_rotation_rate = my_imu.get_y_rate();
  x_rotation_rate = my_imu.get_x_rate();
  
  // Print the returned values for each output on the Serial monitor
  Serial.print("X Acceleration = ");
  Serial.print(x_acc);
  Serial.print(" A/D Value ");
  Serial.print(x_acceleration);
  Serial.println(" g");
  
  Serial.print("Y Acceleration = ");
  Serial.print(y_acc);
  Serial.print(" A/D Value ");
  Serial.print(y_acceleration);
  Serial.println(" g");
  
  Serial.print("Z Acceleration = ");
  Serial.print(z_acc);
  Serial.print(" A/D Value ");
  Serial.print(z_acceleration);
  Serial.println(" g");
  
  Serial.print("Y Rotation Rate = ");
  Serial.print(y_rate);
  Serial.print(" A/D Value ");
  Serial.print(y_rotation_rate);
  Serial.println(" Degrees/Sec");
  
  Serial.print("X Rotation Rate = ");
  Serial.print(x_rate);
  Serial.print(" A/D Value ");
  Serial.print(x_rotation_rate);
  Serial.println(" Degrees/Sec");
  
  // Delay for 1500 mSec so that the ouput can be read on the Serial Monitor
  Serial.println();
  delay(1500);
  
}
