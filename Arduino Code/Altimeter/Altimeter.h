//
// University of Louisville USLI, 2011-2012
//
// Filename		:	Altimeter.h
// Written by	:	Lucas Spicer
// Date			:	14 December 2011
//				:    
// Description 	:	C++ Header file which contains a class defintion for a custom
//					Altimeter class (which is designed to allow for connection to PerfectFlite
//					logging Altimeters (such as the Stratologger and Pnut) when they are
//					configured for telemetry output.
//

#ifndef alt_h
#define alt_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

#define SERIAL0 0
#define SERIAL1 1
#define SERIAL2 2
#define SERIAL3 3
#define NUMCHARS 10
#define LF 10
#define CR 13

class Altimeter
{
	public:
		// Altimeter object Constructors
		Altimeter(int serial_port_num);
		// PUBLIC FUNCTIONS
		int get_altitude(void);
		
	private:
		// PRIVATE FUNCTIONS
		int wait_for_data(void);
#if defined(ARDUINO) && ARDUINO >= 100
#else
		int parse_int(void);
#endif
		// PRIVATE VARIABLES
		int _serial_port_number;
};

#endif