//
// University of Louisville USLI, 2011-2012
//
// Filename		:	Altimeter.cpp
// Written by	:	Lucas Spicer
// Date			:	14 December 2011
// Description 	:	C++ file which contains function definitions for the custom
//					Altimeter class. The constructor allows the device to be attached
//					to any of the 4 hardware serial ports of the arduino mega, or the default
//					single Serial port for the Uno. The function, get_altitude()
//					simply returns the latest altitude of the device in ft above ground level
//					of whatever configuration the logging altimeter is set up for. The serial
//					boad rate is assumed to be 9600 as described in the PerfectFlite altimeter
//					user manuals, but should be adjusted for whatever particular device is being used.
//					A private function to the library, wait_for_data() pauses execution until the altimeter
//					as sent at least 10 bytes of altitude data, or the system times out. If the system
//					times out, it may indicate a impropperly configured, broken, disconnected 
//					or damaged altimeter. An altitude of -999 indicates the device timed out 
//

#include "Altimeter.h"

//****** PUBLIC//******//
//
// Simple Altimeter constructor Altimeter() for use with hardware serial ports
//
// Input:	Serial Port Number
// Return:	N.A.
//
// Description: Creates an Altimeter interface object attached to the specified serial port
//
Altimeter::Altimeter(int serial_port_number) {
  _serial_port_number = serial_port_number;
  if(_serial_port_number == 0) {		// if _serial_port_number = 0, attach to Serial
	Serial.begin(9600);					// default buad rate for PerfectFlite altimeters 9600
#if defined(ARDUINO) && ARDUINO >= 100
	Serial.setTimeout(100);				// Timeout prevents altimeter device malfunction from
#endif									// locking up the entire system
										
  }
  else if(_serial_port_number == 1) {	// if _serial_port_number = 1, attach to Serial1
	Serial1.begin(9600);				// default buad rate for PerfectFlite altimeters 9600
#if defined(ARDUINO) && ARDUINO >= 100
	Serial1.setTimeout(100);			// Timeout prevents altimeter device malfunction from
#endif									// locking up the entire system
  }
  else if(_serial_port_number == 2) {	// if _serial_port_number = 2, attach to Serial1
	Serial2.begin(9600);				// default buad rate for PerfectFlite altimeters 9600
#if defined(ARDUINO) && ARDUINO >= 100
	Serial2.setTimeout(100);			// Timeout prevents altimeter device malfunction from
#endif									// locking up the entire system
  }						
  else if(_serial_port_number == 3) {	// if _serial_port_number = 2, attach to Serial1
	Serial3.begin(9600);				// default buad rate for PerfectFlite altimeters 9600
#if defined(ARDUINO) && ARDUINO >= 100
	Serial3.setTimeout(100);			// Timeout prevents altimeter device malfunction from
#endif									// locking up the entire system
  }
  else {								// else (for other numbers) attach to default Serial
	Serial.begin(9600);					// default buad rate for PerfectFlite altimeters 9600
#if defined(ARDUINO) && ARDUINO >= 100
	Serial.setTimeout(100);				// Timeout prevents altimeter device malfunction from
#endif									// locking up the entire system
  }
}

//
// Function: get_altitude()
// 
// Input: 	void
// Return:	int altitude in ft above ground level (or setting of attached
//			altimeter). If Altimeter becomes disconnected then it the is fucntion will return
//			an altitude of -999 ft, which indicates a timeout of the serial port
//
int Altimeter::get_altitude() {
  int altitude;							// local variable to store altitude in
  if (_serial_port_number == 0) {		// if _serial_port_number = 0, user Serial
	altitude = wait_for_data();			// if wait_for_data returns -999, then altimeter timed out 
#if defined(ARDUINO) && ARDUINO >= 100
	while(Serial.available() >= NUMCHARS) {	// Read until we have the most recent altitude
		altitude = Serial.parseInt();	// use parseInt() to read altidue from serial (for Arduino 1.0+)
	}
#else
	while(Serial.available() >= NUMCHARS) {	// Read unitil we have the most recent altitude available
		Serial.read();
	}
	altitude = parse_int();				// use parse_int() to read altidue from serial (for Arduino 022)
#endif									
  }
  else if (_serial_port_number == 1) {	// if _serial_port_number = 1, user Serial1
	altitude = wait_for_data();			// if wait_for_data returns -999, then altimeter timed out
#if defined(ARDUINO) && ARDUINO >= 100
	while(Serial1.available() > NUMCHARS) {	// Read until we have the most recent altitude
		altitude = Serial1.parseInt();	// use parseInt() to read altidue from serial (for Arduino 1.0+)
	}
#else
	while(Serial1.available() > NUMCHARS) {	// Read unitil we have the most recent altitude available
		if((Serial1.read() == LF) && (Serial1.available() < NUMCHARS*2)) {
			break;
		}
	}
	altitude = parse_int();				// use parse_int() to read altidue from serial (for Arduino 022)
#endif	
  }
  else if (_serial_port_number == 2) {	// if _serial_port_number = 2, user Serial2
	altitude = wait_for_data();			// if wait_for_data returns -999, then altimeter timed out
#if defined(ARDUINO) && ARDUINO >= 100
	while(Serial2.available() > NUMCHARS) {	// Read until we have the most recent altitude
		altitude = Serial2.parseInt();	// use parseInt() to read altidue from serial (for Arduino 1.0+)
	}
#else
	while(Serial2.available() > NUMCHARS) {	// Read unitil we have the most recent altitude available
		if((Serial2.read() == LF) && (Serial2.available() < NUMCHARS*2)) {
			break;
		}
	}
	altitude = parse_int();				// use parse_int() to read altidue from serial (for Arduino 022)
#endif	
  }
  else if (_serial_port_number == 3) {	// if _serial_port_number = 3, user Serial3
	altitude = wait_for_data();			// if wait_for_data returns -999, then altimeter timed out
#if defined(ARDUINO) && ARDUINO >= 100
	while(Serial3.available() > NUMCHARS) {	// Read until we have the most recent altitude
		altitude = Serial3.parseInt();	// use parseInt() to read altidue from serial (for Arduino 1.0+)
	}
#else
	while(Serial3.available() > NUMCHARS) {	// Read unitil we have the most recent altitude available
		if((Serial3.read() == LF) && (Serial3.available() < NUMCHARS*2)) {
			break;
		}
	}
	altitude = parse_int();				// use parse_int() to read altidue from serial (for Arduino 022)
#endif	
  }
  else	{								// if other use Serial
	altitude = wait_for_data();			// if wait_for_data returns -999, then altimeter timed out
#if defined(ARDUINO) && ARDUINO >= 100
	while(Serial.available() > NUMCHARS) {	// Read until we have the most recent altitude
		altitude = Serial.parseInt();	// use parseInt() to read altidue from serial (for Arduino 1.0+)
	}
#else
	while(Serial.available() > NUMCHARS) {	// Read unitil we have the most recent altitude available
		if((Serial.read() == LF) && (Serial.available() < NUMCHARS*2)) {
			break;
		}
	}
	altitude = parse_int();				// use parse_int() to read altidue from serial (for Arduino 022)
#endif
  }
  return altitude;						// return the latest altitude in ft above ground level
}

//
// Function: wait_for_data()
// 
// Input: 	void
// Return:	int (1 if we have data to parse for altitude value, -999 if timeout)
//
int Altimeter::wait_for_data(void) {
  unsigned long timeout = millis();			// timeout time is initialized to the current time
  int isvalid = -999;						// if the device times out -999 (indicating timeout) is returned
  while(millis() - timeout < 100) {			// Altimeter has 100mSec to read value before timeout occurs
	  if (_serial_port_number == SERIAL0) {		// if _serial_port_number = 0, user Serial
		if(Serial.available() > NUMCHARS) {		// Wait until there are at least 10 bytes to read
			isvalid = 1;					// If Serial available >= NUMCHARS device is good so return
			return isvalid;
		}
	  }
	  else if (_serial_port_number == SERIAL1) {	// if _serial_port_number = 1, user Serial1
		if(Serial1.available() > NUMCHARS) {		// Wait until there are at least NUMCHARS bytes to read
			isvalid = 1;					// If Serial available >= NUMCHARS device is good so return
			return isvalid;
		}
	  }
	  else if (_serial_port_number == SERIAL2) {	// if _serial_port_number = 2, user Serial2
		if(Serial2.available() > NUMCHARS) {		// Wait until there are at least NUMCHARS bytes to read
			isvalid = 1;					// If Serial available >= NUMCHARS device is good so return
			return isvalid;
		}
	  }
	  else if (_serial_port_number == SERIAL3) {	// if _serial_port_number = 3, user Serial3
		if(Serial3.available() > NUMCHARS) {		// Wait until there are at least NUMCHARS bytes to read
			isvalid = 1;					// If Serial available >= NUMCHARS device is good so return
			return isvalid;
		}
	  }
	  else	{								// if other use Serial
		if(Serial.available() > NUMCHARS) {		// Wait until there are at least NUMCHARS bytes to read
			isvalid = 1;					// If Serial available >= NUMCHARS device is good so return
			return isvalid;
		}
	  }
  }
  return isvalid;							// If we get to this statement, then timeout occurred
}

#if defined(ARDUINO) && ARDUINO >= 100
#else
//
// Function: parse_int())
// 
// Input: 	void
// Return:	int (integer value parsed from serial stream)
//
int Altimeter::parse_int(void) {
  int isvalid = -999;                               // Flag to determine if we got a valid input, -999 = timout or invalid
  int i = 0;                                        // char_buff index counter
  unsigned long timeout = 0;                        // Variable to store the timeout time           
  char char_buff[NUMCHARS];							// Character buffer to store the characters read in
  timeout = millis();                             	// Initialize the timeout counter
  char c;
  while(i < 2) {									// Loop until we get a least 2 characters for altitude
    if(_serial_port_number == SERIAL0) {
		c = Serial.read();               // Read Serial for next char of altitude
	}
	else if(_serial_port_number == SERIAL1) {
		c = Serial1.read();              // Read Serial1 for next char of altitude
	}
	else if(_serial_port_number == SERIAL2) {
		c = Serial2.read();              // Read Serial2 for next char of altitude
	}
	else if(_serial_port_number == SERIAL3) {
		c = Serial3.read();              // Read Serial3 for next char of altitude
	}
	else {
		c = Serial.read();             	// Read Serial for next char of altitude
	}
	char_buff[i] = (char)c;
  
    while(c != CR && i < 8) {          	// While char != end of line, or have a full buffer, continue
		if(c != -1) {                   // If char != -1 (Serial.read() == -1 indicates no data available
			i++;                        // Increment index buffer
		}                               // This ensure Arduino does not go faster than altimeter transmission
		
		if(_serial_port_number == SERIAL0) {
			c	= Serial.read();       // Read Serial for next char of altitude
		}
		else if(_serial_port_number == SERIAL1) {
			c = Serial1.read();			// Read Serial1 for next char of altitude
		}
		else if(_serial_port_number == SERIAL2) {
			c = Serial2.read();			// Read Serial2 for next char of altitude
		}
		else if(_serial_port_number == SERIAL3) {
			c = Serial3.read();			// Read Serial3 for next char of altitude
		}
		else {
			c = Serial.read();			// Read Serial for next char of altitude
		}
        char_buff[i] = (char)c;
		
		if(millis() - timeout > 50) {              	// If millis() - timeout > 50, this means more than 50mSec pased    
          return isvalid;                           // altimeter dead or dying, return isvalid == -999 (timeout)
        }                                           
    }
  }
  if(char_buff[i] != CR) {							// If last char of altitude is not end of line
	return isvalid;									// return isvalid = -999 (invalid number)
  }
  char_buff[i] = '\0'; 								// Make last character of char_buff[i] = NULL
  return atoi(char_buff);                       	// Use atoi() to convert char array to integer (altitude)
}
#endif