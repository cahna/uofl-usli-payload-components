#define CR 13
#define LF 10

void setup() {
  Serial.begin(9600);
  Serial1.begin(9600);
  long startTime = millis();
  for(int x = 0; x < 200; x++) {
    Serial1.print((int)(-(pow(x-100,2.0)) + 10000));
    Serial1.write(CR);
    Serial1.write(LF);
    delay(100);
  }
  long endTime = millis();
  Serial.print("Time to print = ");
  Serial.println(endTime - startTime);
}

void loop() {
}
