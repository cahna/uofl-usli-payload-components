//
//  University of Louisville USLI, 2011-2012
//
//  Filename    :  altimeter_library_example
//  Written by  :  Lucas Spicer
//  Date        :  14 December 2011
//  Description :  Arduino Example Sketch of the Altimeter library, with a PerfectFlite logging
//                 altimeter attached to Serial1. Any PerfectFlite logging altimeter with telemetry
//                 enabled should work with this library. For adetailed description of the
//                 Altimeter class or its funtions please see the fully commented Altimeter.h
//                 and Altimeter.cpp files in the /libraries/Altimeter folder
//

// Include the Library we wish to use in this example
#include <Altimeter.h>

// Create an instance of our Altimeter class (name the GPS object my_altimeter)
// We have connected the altimeter's telemetry (serial output) to the Arduino
// Serial1 port, however we could attach altimeters to any of the 4 serial ports
Altimeter my_altimeter(SERIAL1);

// All Arduino Sketches require both a setup() and loop() funtions, this is the setup()
void setup() 
{
  // Initialize the Serial port (the one connected to the USB interface) at 9600 for debugging and
  // receiving message on the computer's serial monitor
  Serial.begin(9600);
  
  // Print a friendly message to begin our test!
  Serial.println("***Test of PerfectFlite Altimeter***"); 
}

// All Arduino Sketches require both a setup() and loop() funtions, this is the loop()
void loop()
{
  // Call the Altimeter Function get_altitude() to (you guessed it) get the altitude
  int alt = my_altimeter.get_altitude();
  // If the function returns an altitude of -999 ft it means the device timed out
  if (alt == -999) {
    // When the device times out it could mean the device is not configured properly
    // Or is disconnected or damanged
    Serial.println("Altimeter Disconnected or Dead!");
  }
  // Otherwise the altitude should be accurate
  else {
    // So we display the altitude on the serial monitor
    Serial.println(alt);
  }
  
  // Set this delay to anything you want or comment it out. It's here to simulate
  // the amount of time the rest of the code in a full loop() may take
  delay(250);
}
