//
//  University of Louisville USLI, 2011-2012
//
//  Filename    :  GPS_library_static_test
//  Written by  :  Lucas Spicer
//  Date        :  06 November 2011
//  Description :  Arduino Example Sketch of the GPS library, with static NMEA
//                 message strings used to demonstrate how the parser works.
//                 For detailed description of the GPS class or its funtions please
//                 see the fully commented GPS.h and GPS.cpp files in the /libraries/GPS folder
//

// Include the Library we wish to use in this example
#include <GPS.h>

// Create an instance of our GPS class (name the GPS object my_gps)
GPS my_gps;

// Static NMEA strings for demonstration of the parser
String test_NMEA[] = {"$GPGGA,202759.00,3813.59638,N,08545.82351,W,1,08,2.62,142.1,M,-32.7,M,,*6F",
                      "$GPGSA,A,3,12,13,04,10,25,29,02,05,,,,,3.41,2.62,2.19*07",
                      "$GPGSV,3,1,10,02,65,032,28,04,27,072,30,05,70,201,20,10,44,049,28*72",
                      "$GPGSV,3,2,10,12,42,236,14,13,11,042,27,17,00,129,08,25,37,279,26*70",
                      "$GPGSV,3,3,10,26,08,160,,29,21,311,26*7C",
                      "$GPGLL,3813.59638,N,08545.82351,W,202759.00,A,A*79",  
                      "$GPRMC,202800.00,A,3813.59656,N,08545.82360,W,0.229,319.02,061111,,,A*71",
                      "$GPVTG,319.02,T,,M,0.229,N,0.425,K,A*3E",
                      "$GPGGA,202800.00,3813.59656,N,08545.82360,W,1,09,2.62,142.1,M,-32.7,M,,*67",
                      "$GPGSA,A,3,17,12,13,04,10,25,29,02,05,,,,3.41,2.62,2.19*01",
                      "$GPGSV,3,1,10,02,65,032,28,04,27,072,30,05,70,201,21,10,44,049,28*73",
                      "$GPGSV,3,2,10,12,42,236,14,13,11,042,26,17,00,129,10,25,37,279,26*78",
                      "$GPGSV,3,3,10,26,08,160,,29,21,311,26*7C",
                      "$GPGLL,3813.59656,N,08545.82360,W,202800.00,A,A*70",
                      "$GPRMC,202801.00,A,3813.59639,N,08545.82366,W,0.099,,061111,,,A*61",
                      "$GPVTG,,T,,M,0.099,N,0.183,K,A*29"};

// All Arduino Sketches require both a setup() and loop() funtions, this is the setup()
void setup() 
{ 
  // Initialize the Serial port (the one connected to the USB interface) at 9600 for debugging and
  // receiving message on the computer's serial monitor
  Serial.begin(9600); 
  
  // A friendly message to begin our test! 
  Serial.println("GPS Library Static Test");
  
  // Use the custom read_gps() function defined below to read in and display on the serial monitor
  // each of the Static NMEA strings and all the parsed data they contain
  // Notice that the parser as currently designed only parses NMEA strings of type GGA, GSA and RMC
  // This is because they contain all (and more) than the necessary information most projects may want
  // It add support for additional NMEA message types see the GPS.h and GPS.cpp comments, its easy to do!
  for(int i = 0; i < 16; i++) {
    read_gps(test_NMEA[i]);
    Serial.println();
  }
} 

// All Arduino Sketches require both a setup() and loop() funtions, this is the loop()
void loop() {
}

//
// Function: read_gps()
//
// Input: void
// Return: void
//
// Description: Use this function to read the static NMEA Strings and then display their
// Contents in two ways. By selecting VERBOSE mode, the parse_NMEA_message displays on
// the serial moniton all the fields it just parsed. If you leave this input empty, it
// won't do that. After this the data itself is returned as integer or floats as examples
// of the various ways to gain access or use the data parsed from the NMEA Strings.
//
void read_gps(String nmea_string) {
  // Next we use par_NMEA_message to get the data from the message and validate
  // its checksum. If this function call returns 0 the checksum was invalid, so don't
  // bother getting the data from it.
  int test = my_gps.parse_NMEA_message(nmea_string, VERBOSE);
  if(test == 1) {
    Serial.println("Checksum Valid\n");
    
    // Test prints for latitude object
    Latitude test_latitude = Latitude(my_gps.get_latitude_string());
    Serial.print("Latitude Degrees = ");
    Serial.println(test_latitude.get_degrees());
    Serial.print("Latitude Minutes = ");
    Serial.println(test_latitude.get_minutes());
    Serial.print("Latitude Seconds = ");
    Serial.println(test_latitude.get_seconds(),5);
    Serial.println();
    
    // Test prints for longitude object
    Longitude test_longitude = Longitude(my_gps.get_longitude_string());
    Serial.print("Longitude Degrees = ");
    Serial.println(test_longitude.get_degrees());
    Serial.print("Longitude Minutes = ");
    Serial.println(test_longitude.get_minutes());
    Serial.print("Longitude Seconds = ");
    Serial.println(test_longitude.get_seconds(),5);
    Serial.println();
    
    // Test prints for Time object
    Time test_time = Time(my_gps.get_time_string());
    Serial.print("Time Hours = ");
    Serial.println(test_time.get_hours());
    Serial.print("Time Minutes = ");
    Serial.println(test_time.get_minutes());
    Serial.print("Time Seconds = ");
    Serial.println(test_time.get_seconds(),5);
    Serial.println(test_time.get_formatted_time());
    Serial.println();
    
    // Test prints for Date object
    Date test_date = Date(my_gps.get_date_string());
    Serial.print("Date Day = ");
    Serial.println(test_date.get_day());
    Serial.print("Date Month = ");
    Serial.println(test_date.get_month());
    Serial.print("Date Year = ");
    Serial.println(test_date.get_year());
    Serial.println(test_date.get_formatted_date());
    Serial.println();
    
    // Test prints for GPS value get functions
    Serial.print("Altitude = ");
    Serial.print(my_gps.get_altitude());
    Serial.println(my_gps.get_altu_string());
    
    Serial.print("Course = ");
    Serial.println(my_gps.get_course());
    
    Serial.print("Number of Satellites Used = ");
    Serial.println(my_gps.get_nosat());
    
    Serial.print("Speed = ");
    Serial.print(my_gps.get_speed(KNOTS));
    Serial.println(" knots");
    
    Serial.print("Speed = ");
    Serial.print(my_gps.get_speed(MPH));
    Serial.println(" mph");
    
    // Test Status Indicators
    if(my_gps.get_data_status() == 0) {
      Serial.println("Data is invalid!");
    }
    else if(my_gps.get_data_status() == 1) {
      Serial.println("Data is valid");
    }
    else {
      Serial.println("Data status unknown");
    }
    
    if(my_gps.get_fix_mode() == 1) {
      Serial.println("No Fix Available");
    }
    else if(my_gps.get_fix_mode() == 2) {
      Serial.println("2D Fix Available");
    }
    else if(my_gps.get_fix_mode() == 3){
      Serial.println("3D Fix Available");
    }
    else {
      Serial.println("Fix Mode unknown");
    }
    
    if(my_gps.get_fix_status() == 0) {
      Serial.println("No Fix Available");
    }
    else if(my_gps.get_fix_status() == 1) {
      Serial.println("Standard GPS Fix Available");
    }
    else {
      Serial.println("Fix Status Unknown");
    }
    
    if(my_gps.get_mode_ind() == 0) {
      Serial.println("Mode Indicates 'No Fix'");
    }
    else {
      Serial.println("Mode Indicates a fix");
    }
    
    Serial.println();
  }
  // If parse_NMEA_message() returns -1 the message was not of valid type
  else if(test == -1) {
    Serial.println("Message was not of type GGA, GSA or RMC");
  }
  // If parse_NMEA_message() returns 0 the message checksum was invalid
  else {
    Serial.println("Checksum Invalid\n");
  }
}
