//
// University of Louisville USLI, 2011-2012
//
// Filename			: GPS.cpp
// Written by		: Lucas Spicer
// Date				: 06 November 2011, updated 15 January 2012
// Description    	: C++ file which contains function defintions for a custom
//					GPS class (which is a fully functional standard UART interface 
//					NMEA GPS message parser). Currently the GPS class supports full
//					parsing of NMEA message types GGA, GSA and RMC. To add support for
//					additional message types the public function parse_NMEA_message
//					should be updated for the new message type and private variables
//					should be added to support any additional data fields not contained
//					in the existing message types. The parser also supports checksum
//					verification of all input NMEA strings. By default this parser assumes
//					the GPS modules is attached to the Arduino Serial2 interface. To attach
//					your GPS device to a different serial port, simple modify the three calls
//					to Serial2.read() in the read_gps_line() function to whatever hardware
//					or softserial device you desire. 
//
#include "GPS.h"

//******* PUBLIC *******//

//
// GPS class constructor GPS()
//
// Input: void
// Return: creates GPS object
//
// Description: Use this function to create a new GPS object, also the constructor also
//				initializes the NMEA field arrays with the pointers to their various field character arrays
//				also selects the proper serial port the GPS is attached to
//
GPS::GPS() {
  // Initialize the array of pointers to the field strings for each NMEA message (there may be better way to do this)
  
  // Initialize the GPGGA field pointers
  _GPGGA_fields[0]    = _time;
  _GPGGA_fields[1]    = _latitude;
  _GPGGA_fields[2]    = _n_s;
  _GPGGA_fields[3]    = _longitude;
  _GPGGA_fields[4]    = _e_w;
  _GPGGA_fields[5]    = _fs;
  _GPGGA_fields[6]    = _nosat;
  _GPGGA_fields[7]    = _hdop;
  _GPGGA_fields[8]    = _alt;
  _GPGGA_fields[9]    = _altu;
  _GPGGA_fields[10]   = _altref;
  _GPGGA_fields[11]   = _usep;
  _GPGGA_fields[12]   = _diffage;
  _GPGGA_fields[13]   = _diffstation;
  _GPGGA_fields[14]   = _cs_gga;
  
  // Initialize the GPGSA field pointers
  _GPGSA_fields[0]    = _smode;
  _GPGSA_fields[1]    = _mode;
  _GPGSA_fields[2]    = _sv1;
  _GPGSA_fields[3]    = _sv2;
  _GPGSA_fields[4]    = _sv3;
  _GPGSA_fields[5]    = _sv4;
  _GPGSA_fields[6]    = _sv5;
  _GPGSA_fields[7]    = _sv6;
  _GPGSA_fields[8]    = _sv7;
  _GPGSA_fields[9]    = _sv8;
  _GPGSA_fields[10]   = _sv9;
  _GPGSA_fields[11]   = _sv10;
  _GPGSA_fields[12]   = _sv11;
  _GPGSA_fields[13]   = _sv12;
  _GPGSA_fields[14]   = _pdop;
  _GPGSA_fields[15]   = _hdop;
  _GPGSA_fields[16]   = _vdop;
  _GPGSA_fields[17]   = _cs_gsa;
 
  // Initialize the GPRMC field pointers
  _GPRMC_fields[0]    = _time;
  _GPRMC_fields[1]    = _status;
  _GPRMC_fields[2]    = _latitude;
  _GPRMC_fields[3]    = _n_s;
  _GPRMC_fields[4]    = _longitude;
  _GPRMC_fields[5]    = _e_w;
  _GPRMC_fields[6]    = _speed;
  _GPRMC_fields[7]    = _cog;
  _GPRMC_fields[8]    = _date;
  _GPRMC_fields[9]    = _mv;
  _GPRMC_fields[10]   = _mve;
  _GPRMC_fields[11]   = _modeind;
  _GPRMC_fields[12]   = _cs_rmc;
  
  // Initialize the GPVTG field pointers
  _GPVTG_fields[0]    = _cog;
  _GPVTG_fields[1]    = _t;
  _GPVTG_fields[2]    = _cogm;
  _GPVTG_fields[3]    = _m;
  _GPVTG_fields[4]    = _speed;
  _GPVTG_fields[5]    = _n;
  _GPVTG_fields[6]    = _speed_kph;
  _GPVTG_fields[7]    = _k;
  _GPVTG_fields[8]    = _modeind;
  _GPVTG_fields[9]    = _cs_rmc;
}

// PUBLIC GPS "Get" methods

//
// Function: read_gps_line()
//
// Input: char char_buff[] (character array buffer we wish to read GPS string into)
// Return: int isvalid (isvalid = 0 if invalid string returned, not $,,,,*CS\n\r,
//   isvalid = 1 if a valid NMEA string is read into char_buff[])
//
// Description: Use this function read Serial port for NMEA strings and store them as
//   char array char_buff[] for further parsing. Valids input string to ensure it is of format
//   $,,...data...,,*CS\n\r and returns a flag to indicate if string was valid
//   TO DO: Add timeout function so that if data stops being input function does not lockup!
//
int GPS::read_gps_line() {
  int isvalid = 0;                                  // Flag to determine if we got a valid input 1 = valid, -1 = timeout, 0 = invalid
  int i = 0;                                        // char_buff index counter
  unsigned long timeout = millis();                 // Variable to store the timeout time
  _char_buff[0] = Serial2.read();                   // Read serial into char_buff index 0
  while (_char_buff[0] != '$') {                    // Read Until char_buff[0] == '$' to indicate we're starting a valid string
	_char_buff[0] = Serial2.read();					// Read serial into char_buff index 0
	if(millis() - timeout > 150) {              	// If millis() - timeout > 150, this means more than 150mSec      
	    isvalid = -1;                             	// have passed without '$' seen since on serial line, so GPS unit is
        return isvalid;                           	// dead or dying, return isvalid == -1 (timeout)
    }
  }
  i++;                                            	// Increment char_buff index counter by 1
  _char_buff[i] = Serial2.read();                 	// Read Serial again for next char
  timeout = millis();                             	// Initialize the timeout counter again
  while(_char_buff[i] != '\n' & i < 85){         	// While char does not equal end of line, or have a full buffer, continue
	if(_char_buff[i] != -1) {                  		// If char != -1 (Serial.read() == -1 indicates no data available
	  i++;                                      	// Increment index buffer
	}                                          		// This ensures Arduino does not go faster than GPS transmission
	_char_buff[i] = Serial2.read();              	// Read in next character in char_buff[i]
	if(millis() - timeout > 150) {              	// If millis() - timeout > 150, this means more than 150mSec      
	  isvalid = -1;                             	// have passed since '$' was seen on on serial line, so GPS unit is
	  return isvalid;                           	// dead or dying, return isvalid == -1 (timeout)
	}                                           
  }
  if(_char_buff[i] == '\n') {                     	// If the last character we read was the end of line characer
	isvalid = 1;                                  	// Then the data is valid. Else we just ran out of buffer
  }                                               	// So valid stays equal to 0
  _char_buff[i] = 0; 								// Make last character of char_buff[i] = NULL
													// So we have a null terminated string for conversion
													// to string object or for serial printing
  return isvalid;                                   // Return status (0 if invalid, 1 if valid)
}

//
// Function: get_gps_line()
//
// Input: void
//
// Return: String 
//
// Description: Use this function to converts whatever is in the private global _char_buff
//				and returns it as a String object
//
String GPS::get_gps_line() {
  return String(_char_buff);
}

//
// Function: parse_NMEA_message()
//
// Input: String message (the NMEA message to parse for delimiters and data substrings)
//		  integer isVERBOSE (default is 0, set to 1 to enable Serial debug output of NMEA information)
// Return: 	int (if parse_NMEA_message() returns 1, it means a valid checksum for message type GGA, GSA or RMC
//			was parsed. Else if return -1 a valid checksum was parsed, but message was of a type other than GGA, GAS or
//			RMC. Else the checkum was invalid, so ignore the data. The data is not parsed unless the return is 1)
//
// Description: Use this function take a NMEA style message String and parse it as a way
//  to populate the various String data fields that the GPS measures
//
int GPS::parse_NMEA_message(String message) {
  int isvalid = 0;
  if(verify_checksum(message) == 1) {
      isvalid = 1;
  }
  else {
      return isvalid;
  }
  if(message.startsWith("$GPGGA")) {
	int num_delimiters = parse_delimiter_indexes(message, _GPGGA_indexes);
    parse_field_substrings(message, _GPGGA_indexes, num_delimiters, _GPGGA_fields);
	
	#if defined(VERBOSE)	
		Serial.println(message);
		Serial.println(" is a $GPGGA NMEA message");
		print_field_substrings(_GPGGA_fields, 1, num_delimiters);
	#endif
  }
  else  if(message.startsWith("$GPGSA")) {
    int num_delimiters = parse_delimiter_indexes(message, _GPGSA_indexes);
    parse_field_substrings(message, _GPGSA_indexes, num_delimiters, _GPGSA_fields);
	#if defined(VERBOSE)
		Serial.println(message);
		Serial.println(" is a $GPGSA NMEA message");
		print_field_substrings(_GPGSA_fields, 2, num_delimiters);
	#endif
  }
  else  if(message.startsWith("$GPRMC")) {
    int num_delimiters = parse_delimiter_indexes(message, _GPRMC_indexes);
    parse_field_substrings(message, _GPRMC_indexes, num_delimiters, _GPRMC_fields);
	#if defined(VERBOSE)
		Serial.println(message);
		Serial.println(" is a $GPRMC NMEA message");
		print_field_substrings(_GPRMC_fields, 3, num_delimiters);
	#endif
  }
  else if(message.startsWith("$GPVTG")) {
	int num_delimiters = parse_delimiter_indexes(message, _GPVTG_indexes);
    parse_field_substrings(message, _GPVTG_indexes, num_delimiters, _GPVTG_fields);
	#if defined(VERBOSE)
		Serial.println(message);
		Serial.println(" is a $GPVTG NMEA message");
		print_field_substrings(_GPVTG_fields, 4, num_delimiters);
	#endif
  }
  else {
    isvalid = -1;
    return isvalid;
  }
  return isvalid;
}

//   All GPS "get_variable_String" functions have same form
//
// Function: get_"variable name"_String()
//
// Input: void
// Return: String object with contents of "variable name" character array in String
//
// Description: Use this function to get the raw contents of the character array string
//   for the given "variable name" which labels that given GPS data field. The data is
//   returned as an arduino String object because returning a pointer to the character array
//   does not protect the private status of that data string
//

String GPS::get_alt_string() {
  return String(_alt);;
}

String GPS::get_altref_string() {
  return String(_altref);
}

String GPS::get_altu_string() {
  return String(_altu);
}

String GPS::get_cog_string() {
  return String(_cog);
}

String GPS::get_cs_gga_string() {
  return String(_cs_gga);
}

String GPS::get_cs_gsa_string() {
  return String(_cs_gsa);
}

String GPS::get_cs_rmc_string() {
  return String(_cs_rmc);
}

String GPS::get_cs_vtg_string() {
  return String(_cs_vtg);
}

String GPS::get_date_string() {
  return String(_date);
}

String GPS::get_diffage_string() {
  return String(_diffage);
}

String GPS::get_diffstation_string() {
  return String(_diffstation);
}

String GPS::get_e_w_string() {
  return String(_e_w);
}

String GPS::get_fs_string() {
  return String(_fs);
}

String GPS::get_hdop_string() {
  return String(_hdop);
}

String GPS::get_latitude_string() {
  return String(_latitude);
}

String GPS::get_longitude_string() {
  return String(_longitude);
}

String GPS::get_mode_string() {
  return String(_mode);
}

String GPS::get_modeind_string() {
  return String(_modeind);
}

String GPS::get_mv_string() {
  return String(_mv);
}

String GPS::get_mve_string() {
  return String(_mve);
}

String GPS::get_n_s_string() {
  return String(_n_s);
}

String GPS::get_nosat_string() {
  return String(_nosat);
}

String GPS::get_pdop_string() {
  return String(_pdop);
}

String GPS::get_smode_string() {
  return String(_smode);
} 

String GPS::get_speed_string() {
  return String(_speed);
}

String GPS::get_status_string() {
  return String(_status);
}

String GPS::get_sv1_string() {
  return String(_sv1);
}

String GPS::get_sv2_string() {
  return String(_sv2);
}

String GPS::get_sv3_string() {
  return String(_sv3);
}

String GPS::get_sv4_string() {
  return String(_sv4);
}

String GPS::get_sv5_string() {
  return String(_sv5);
}

String GPS::get_sv6_string() {
  return String(_sv6);
}

String GPS::get_sv7_string() {
  return String(_sv7);
}

String GPS::get_sv8_string() {
  return String(_sv8);
}

String GPS::get_sv9_string() {
  return String(_sv9);
}

String GPS::get_sv10_string() {
  return String(_sv10);
}

String GPS::get_sv11_string() {
  return String(_sv11);
}

String GPS::get_sv12_string() {
  return String(_sv12);
}

String GPS::get_time_string() {
  return String(_time);
}

String GPS::get_usep_string() {
  return String(_usep);
}

String GPS::get_vdop_string() {
  return String(_vdop);
}

//   All GPS "get_variable" functions have same form
float GPS::get_altitude() {                         // Returns the altitude of the GPS above mean sea level
  return atof(_alt);                                // Use atof() to convert char array to float
}

float GPS::get_course() {                           // Returns the course of the GPS relative to true N in degrees
  return atof(_cog);                                // Use atof() to convert char array to float
}

int GPS::get_nosat() {                              // Returns the number of satalies used to calculate fix
  return atoi(_nosat);                              // Use atoi() to convert char array to integer
}

// Quality,Status,Fix Indicator Variable "get_status" functions

int GPS::get_data_status() {
  if (_status[0] == 'V') {                          // Status = V = Navigation receiver warning
    return 0;                                       // return 0 = data invalid
  }
  else if (_status[0] == 'A') {                     // Status = A = = Data Valid
    return 1;                                       // return 1 = data valid
  }
}

int GPS::get_fix_mode()  {                          // Mode = 1 No Fix/Invalid
  return atoi(_mode);                               // Mode = 2 2D Fix Valid, Mode = 3 3D Fix Valid
}

int GPS::get_fix_status() {                         // FS = 0 No Fix/Invalid
  return atoi(_fs);                                 // FS = 1 Standard GPS, Fix Valid (2D/3D)                            
}

int GPS::get_mode_ind() {                                
  if (_modeind[0] == 'N') {                         // Mode Indicator = N = No Fix
    return 0;                                       // Return 0 = No fix
  }
  else {                                            // Else there is some sort of fix!
    return 1;
  }
}

//
// Function: get_speed()
//
// Input: int speed_units (use constants KNOTS, MPH, MPS, KPH) chooses what units to return
//		  the speed in, default is knots (if no units chosen funtion returns speed in knots)
// Return: float (the speed of the GPS over ground in the specified units)
//
// Description: Use this function to return a float of the speed of the GPS over ground in
// 				units of knots (KNOTS), miles per hour (MPH), meters per second (MPS)
//				or kilometers per hour (KPH)
//
float GPS::get_speed(int speed_units) {
  float temp_speed = atof(_speed);					// Use atof() to convert char array to float
  switch (speed_units) {
	case 0:
		break;										// Default is speed of GPS over ground in knots
	case 1:
		temp_speed = temp_speed*1.151;              // Convert speed to mph and break     
		break;										
	case 2:
		temp_speed = temp_speed*0.514;              // Convert speed to mps and break
		break;
	case 3:
		temp_speed = temp_speed*1.852;              // Convert speed to kph and break
		break;
	default:
		break;
  }
  return temp_speed;								// Returns the speed over ground of the GPS
}

//******* PRIVATE *******//

// Initialization of the Static Arrays which hold the labels for the fields present in each NMEA message type
const char GPS::_GPGSA_labels[][15] = {	"Smode","Pos Fix Mode","SV  1","SV  2","SV  3","SV  4","SV  5","SV  6","SV  7","SV  8",
										"SV  9","SV  10","SV  11","SV  12","PDoP","HDoP","VDoP","GPGSA Checksum"};

const char GPS::_GPRMC_labels[][15] = {	"Current Time","Status","Latitude","N/S Indicator","Longitude","E/W Indicator",
										"Speed","Course","Date","MagVar, N.A.",
										"MagVar E/W","Mode Ind","GPRMC Checksum"};
										
const char GPS::_GPGGA_labels[][15] = {	"Current Time","Latitude","N/S Indicator","Longitude","E/W Indicator","Pos Fix Status",
										"Num Satellites","Horz DoP","MSL Altitude","Altitude Units",
										"Geoid Sep","GeoidSep Units","Age DiffCor",
										"DiffRefStat ID","GPGGA Checksum"};
										
const char GPS::_GPVTG_labels[][15] = {	"Course","T","CoG Mag","M","Speed","N",
										"Speed kph","KPH","Mode Indicator","GPVTG Checksum"};
								
//
// Function: parse_delimiter_indexes()
//
// Input: String message_string (the NMEA sentence you wish to parse, as an Arduino String object)
//        int index_array[] (the int array the delimiter indexes will be stored in)
// Return: int num_delim (the number of delimters, both "," and "*" found in the message)
//
// Description: Use this function to save the indexes of the NMEA field delimiters into a passed
//   array and also to return the number of delimters in the message
//
int GPS::parse_delimiter_indexes(String message_string, int index_array[]) {
  int index = 0;                                  // index is number of delimter index in string
  int last_index = 0;                             // last index is previous index number
  int num_delim = 0;                              // counter for number of delimiters parsed
  while(1) {                                      // loop until index == -1 which breaks while(1)
    last_index = index;                           // set last_index = index, prior to updating index
    index = message_string.indexOf(",",index+1);  // index = indexOf next "," in the message string
    index_array[num_delim] = index;               // add value of delimiter index to index_array
    if(index == -1) {                             // if index == -1, no "," delimiter found
      break;                                      //   therefore break from this while(1) loop
    }
    num_delim++;                                  // increment the num of delims by 1 each loop    
  }
  index = last_index;                             // update index with the last index (now index != -1)
  while(1) {                                      // loop until index == -1 which breaks while(1)
    index = message_string.indexOf("*",index+1);  // index = indexOf next "*" in the message string
    index_array[num_delim] = index;               // add value of delimiter index to index_array                                  
    if(index == -1) {                             // if index == -1, no "," delimiter found
      break;                                      //   therefore break from this while(1) loop
    }
    num_delim++;                                  // increment the num of delims by 1 each loop
  }
  return num_delim;                               // return the number of delimiters found
}

//
// Function: parse_field_substrings()
//
// Input: String message_string (the NMEA sentence you wish to parse, as an Arduino String object)
//        int index_array[] (the int array the delimiter indexes are stored in)
//        int num_fields (the num of fields, this is the output of parse_delimiter_indexes() function)
//        String fields[] (the array of strings which contains the fields for a given NMEA message type)
// Return: Void
//
// Description: Use this function to parse the substrings from the NMEA message and store them in
//  the appropriate fields
//
void GPS::parse_field_substrings(String message_string, int index_array[], int num_fields, char *fields[]) {
  int i = 0;
  String temp_string;
  int buff_length;  
  for(i; i < num_fields-1; i++) {
    // the current field = the substring located between the next two field delimiter indexes
    temp_string = message_string.substring(index_array[i]+1, index_array[i+1]);
    buff_length = index_array[i+1] - (index_array[i]);
    temp_string.toCharArray(fields[i],buff_length);
  }
  // the final field = the substring located from the last delimiter until the end of the message
  temp_string = message_string.substring(index_array[i]+1);
  buff_length = message_string.length() - (index_array[i]);
  temp_string.toCharArray(fields[i],buff_length);
}

//
// Function: print_field_substrings()
//
// Input: String fields[] (the array of strings which contains the fields for a given NMEA message type)
//        String labels[] (the array of strings which label the fields for a given NMEA message tyep)
//        int num_fields (the num of fields, this is the output of parse_delimiter_indexes() function)
// Return: Void
//
// Description: Use this function to print the contents of a messages fields, along with the labels
//  for those fields
//
void GPS::print_field_substrings(char* fields[], int label_number, int num_fields) {
  for (int i; i < num_fields; i++) {
	if(label_number == 1){
		Serial.print(_GPGGA_labels[i]);
	} else if (label_number == 2){
		Serial.print(_GPGSA_labels[i]);
	} else if (label_number == 3){
		Serial.print(_GPRMC_labels[i]);
	} else if (label_number == 4){
		Serial.print(_GPVTG_labels[i]);
	}
    Serial.print(" = ");
    Serial.println(fields[i]);
  }
}

//
// Function: verify_checksum()
//
// Input: String message (the NMEA message we want to vefigy the checksum of)
//        
// Return: int (1 = valid checksum, 0 = invalid checksum)
//
// Description: Use this function vefiy that the checksum value in the message, matches
//   the calculated checksum value for  the characters recieved
//
int GPS::verify_checksum(String message) {
  char checksum = 0;                                // char to hold value of calucated checksum
  char char_checksum[3];                            // 3 character buffer to hold simple checksum substring
  int simple_string_length;                         // length of char array string version of message
  int converted_checksum;                           // Integer, value of checksum substring
  String String_checksum;                           // String object to contain checksum substring
  
  simple_string_length = message.indexOf("*");                  // Calculate index of checksum substring
  String_checksum = message.substring(simple_string_length+1);  // Parse NMEA message for checksum substring
  
  message.toCharArray(_char_buff,simple_string_length+1);    // Convert message to char array, store in _char_buff
  
  for(int i = 1; i <  simple_string_length; i++) {  // For each character in simple string between '$' and '*'
    checksum ^= _char_buff[i];                   // Calculate checksum as checksum = checksum XOR _char_buff[i]
  }
  String_checksum.toCharArray(char_checksum, 3);    // Convert the message checksum String to char array
  // Calculate value of message substring char array, by converting hex char to integer value
  converted_checksum = 16 * convert_from_hex(char_checksum[0]) + convert_from_hex(char_checksum[1]);
  if(checksum == converted_checksum) {              // If our calcuated checksum == given converted checksum
    return 1;                                       // Return 1 (this means checksum is verfied successfully)
  }
  else {                                            
    return 0;                                       // Else return 0 (this means checksum is not valid)
  }
}

//
// Function: convert_from_hex()
//
// Input: char c (character we wish to convert from hex)
// Return: int (integer value of c as if it were HEX, not char)
//
// Description: Use this function to take a char between 0-9 or a-f or A-F and 
//   convert it to the integer equivalent of that hex value
int GPS::convert_from_hex(char c) 
{
  if (c >= 'A' && c <= 'F') {
    return c - 'A' + 10;
  }
  else if (c >= 'a' && c <= 'f') {
    return c - 'a' + 10;
  }
  else {
    return c - '0';
  }
}