//
// University of Louisville USLI, 2011-2012
//
// Filename		: 	Latitude.cpp
// Written by	: 	Lucas Spicer
// Date			: 	06 November 2011
// Description	: 	C++ file which contains function definitions for the custom
//					Latitude class. The Latitude constructor takes a NMEA
//					latitude submessage formatted as a String object as its input
//					and parses (with full precision) the latitude into seperate
//					degree (integer), minute (integer) and seconds (float) of latidue
//					variables. Simple get_"variable name" functions are used to return
//					the values.
//					
#include "GPS.h"

//
// Latitude Object Constructor
//   Pass the constructor a string representing a NMEA GPS latitude string of standard form
//   3,2.5 where the latitude string had 3 digits for the degrees, 2 for integer number of minutes
//   and 5 places after the decimal for fractional seconds. This string is then parsed and the 
//   variables _degree, _minutes and _seconds are populated with the numeric equivilent
//
Latitude::Latitude(String latitude_string) {
  String temp_string;                               // Temp string to hold parsed sections of latitude_string
  char temp_char_array[10];                         // Temp char array for use as go between String object and atoi() or atof()
  int decimal_index;                                // The index location of the decimal point in the latitude string  
  decimal_index = latitude_string.indexOf(".");
  temp_string = latitude_string.substring(0,decimal_index-2); // First substring = degrees, values up to 2 places before point
  temp_string.toCharArray(temp_char_array,decimal_index-1);   // Convert degrees substring to char array
  _degrees = atoi(temp_char_array);                 // Use atoi() to convert char array to integer, store in _degrees
  
  temp_string = latitude_string.substring(decimal_index-2,decimal_index);     // Second substring = minutes, next two places
  temp_string.toCharArray(temp_char_array,(decimal_index+1)-decimal_index-2); // Convert minutes substring to char array
  _minutes = atoi(temp_char_array);                 // Use atoi() to convert char array to integer, store in _minutes               
  
  temp_string = latitude_string.substring(decimal_index);     // Final substring = fractional minutes
  temp_string.toCharArray(temp_char_array,latitude_string.length()+1-decimal_index); // Convert substring to char array
  _seconds = atof(temp_char_array);                 // Use atof() to convert char array to float, store in _seconds                 
  _seconds = _seconds * 60;                         // Multiply fractional seconds by 60 to convert to fractional seconds
}

//
// Public "Get" Methods for Latitude Object
//
int Latitude::get_degrees() {
  return _degrees;
}

int Latitude::get_minutes() {
  return _minutes;
}

float Latitude::get_seconds() {
  return _seconds;
}