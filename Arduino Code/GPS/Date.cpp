//
// University of Louisville USLI, 2011-2012
//
// Filename		: 	Date.cpp
// Written by	: 	Lucas Spicer
// Date			: 	06 November 2011
// Description	: 	C++ file which contains function definitions for the custom
//					Date class. Date constructor takes a NMEA
//					date submessage formatted as a String object as its input
//					and parses out the day of the month (integer), month (integer) and 
//					year (integer) from the String. Simple get_"variable" name functions
//					are used to return the values. In addition the get_formatted_date()
//					function can be used to return the date as a String of the form
//					"FulllMonthName DD, YYYY"
//	

# include "GPS.h"

//
// Date Object Constructor
//   Pass the constructor a string representing a NMEA GPS Date string, this string is then parsed and the 
//   variables _day, _month, _year and _formatted_date_string are populated with the numeric equivilent
//
Date::Date(String date_string) {
  String temp_string;                               // Temp string to hold parsed sections of longitude_string
  char temp_char_array[3];                          // Temp char array for use as go between String object and atoi() or atof()
  temp_string = date_string.substring(0,2);         // First substring = day, always first 2 values
  temp_string.toCharArray(temp_char_array,3);       // Convert day substring to char array of length 2 + 1 (for NULL terminator)
  _day = atoi(temp_char_array);                     // Use atoi() to convert char array to integer, store in _day
  
  temp_string = date_string.substring(2,4);         // Second substring = month, next two places
  temp_string.toCharArray(temp_char_array,3);       // Convert month substring to char array
  _month = atoi(temp_char_array);                   // Use atoi() to convert char array to integer, store in _month               
  
  temp_string = date_string.substring(4,6);         // Final substring = seconds
  temp_string.toCharArray(temp_char_array,3);       // Convert year substring to char array
  _year = atoi(temp_char_array);                    // Use atoi() to convert char array to integer, store in _year
}

//
// Public "Get" Methods for Date Object
//
int Date::get_day() {
  return _day;
}

int Date::get_month() {
  return _month;
}

int Date::get_year() {
  return _year;
}

// Returns the Date as a formatted string of form: "Month Day, Year"
String Date::get_formatted_date() {
  String temp_date_string;
  switch (_month) {
    case 1:
      temp_date_string = String("January ");
      break;
    case 2:
      temp_date_string = String("February ");
      break;
    case 3:
      temp_date_string = String("March ");
      break;
    case 4:
      temp_date_string = String("April ");
      break;
    case 5:
      temp_date_string = String("May ");
      break;
    case 6:
      temp_date_string = String("June ");
      break;
    case 7:
      temp_date_string = String("July ");
      break;
    case 8:
      temp_date_string = String("August ");
      break;
    case 9:
      temp_date_string = String("September ");
      break;
    case 10:
      temp_date_string = String("October ");
      break;
    case 11:
      temp_date_string = String("November ");
      break;
    case 12:
      temp_date_string = String("December ");
      break;
    default:
      temp_date_string = String("Unknown Month ");
  }
  temp_date_string.concat(_day);
  temp_date_string.concat(", 20");
  temp_date_string.concat(_year);
  return temp_date_string;
}