//
// University of Louisville USLI, 2011-2012
//
// Filename		: 	Time.cpp
// Written by	: 	Lucas Spicer
// Date			: 	06 November 2011
// Description	: 	C++ file which contains function definitions for the custom
//					Time class. Time constructor takes a NMEA
//					time submessage formatted as a String object as its input
//					and parses (with full precision) the hour (integer),
//					minute (integer) and second (float) from the String. Simple 
//					get_"variable" name functions are used to return the values.
//					In addition the get_formatted_time() function can be used to
//					return the time as a String of the form
//					"HH Hours MM Minutes SS.SSSS Seconds"
//

#include "GPS.h"

//
// Time Object Constructor
//   Pass the constructor a string representing a NMEA GPS Time string, This string is then parsed and the 
//   variables _hours, _minutes, _seconds and _formatted_time_string are populated with the numeric equivilent
//
Time::Time(String time_string) {
  String temp_string;                               // Temp string to hold parsed sections of longitude_string
  char temp_char_array[7];                          // Temp char array for use as go between String object and atoi() or atof()
  temp_string = time_string.substring(0,2);         // First substring = hours, always first 2 values
  temp_string.toCharArray(temp_char_array,3);       // Convert hours substring to char array of length 2 + 1 (for NULL terminator)
  _hours = atoi(temp_char_array);                   // Use atoi() to convert char array to integer, store in _hours
  _formatted_time_string = String(temp_string + " Hours ");
  //_formatted_time_string = String(temp_string + ":");
  
  temp_string = time_string.substring(2,4);         // Second substring = minutes, next two places
  temp_string.toCharArray(temp_char_array,3);       // Convert minutes substring to char array
  _minutes = atoi(temp_char_array);                 // Use atoi() to convert char array to integer, store in _minutes               
  _formatted_time_string += temp_string;
  _formatted_time_string += " Minutes ";
  //_formatted_time_string += ":";
  
  temp_string = time_string.substring(4);           // Final substring = seconds
  temp_string.toCharArray(temp_char_array,time_string.length()+1-4); // Convert substring to char array
  _seconds = atof(temp_char_array);                 // Use atof() to convert char array to float, store in _seconds
  _formatted_time_string += temp_string;
  _formatted_time_string += " Seconds "; 
}

//
// Public "Get" Methods for Time Object
//
int Time::get_hours() {
  return _hours;
}

int Time::get_minutes() {
  return _minutes;
}

float Time::get_seconds() {
  return _seconds;
}

String Time::get_formatted_time() {
  return _formatted_time_string;
}