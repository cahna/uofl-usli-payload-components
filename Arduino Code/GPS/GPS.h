//
// University of Louisville USLI, 2011-2012
//
// Filename		:	GPS.h
// Written by	:	Lucas Spicer
// Date			:	06 November 2011, updated 15 January 2012
// Description 	:	C++ Header file which contains a class defintion for a custom
//					GPS class (which is a fully functional standard UART interface 
//					NMEA GPS message parser). Currently the GPS class supports full
//					parsing of NMEA message types GGA, GSA and RMC. To add support for
//					additional message types the public function parse_NMEA_message
//					should be updated for the new message type and private variables
//					should be added to support any additional data fields not contained
//					in the existing message types. The parser also supports checksum
//					verification of all input NMEA strings.
//
//					This header also contains class defintions for Latitude, Longitude,
//					Date and Time classes. The additional classes are used to privided
//					encapsulated access to the full precision contents of the latitude,
//					longitude, date and time portions of the NMEA messages
//
//					Class functions, both Public and Private are fully described in the
//					corresponding .cpp files
//
#ifndef GPS_h
#define GPS_h

//#define VERBOSE 1
#define KNOTS 0
#define MPH 1
#define MPS 2
#define KPH 3

#if defined(ARDUINO) && ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif
  
class GPS
{
	public:
		// GPS object Constructor
		GPS();
		// PUBLIC FUNCTIONS
		int read_gps_line();
		int parse_NMEA_message(String message);
		String get_gps_line();
		
		String get_alt_string();
		String get_altref_string();
		String get_altu_string();
		String get_cog_string();
		String get_cs_gga_string();
		String get_cs_gsa_string();
		String get_cs_rmc_string();
		String get_cs_vtg_string(); 
		String get_date_string();
		String get_diffage_string();
		String get_diffstation_string();
		String get_e_w_string();
		String get_fs_string();
		String get_hdop_string();
		String get_latitude_string();
		String get_longitude_string();
		String get_mode_string();
		String get_modeind_string();
		String get_mv_string();
		String get_mve_string();
		String get_n_s_string();
		String get_nosat_string();
		String get_pdop_string();
		String get_smode_string();
		String get_speed_string();
		String get_status_string();
		String get_sv1_string();
		String get_sv2_string();
		String get_sv3_string();
		String get_sv4_string();
		String get_sv5_string();
		String get_sv6_string();
		String get_sv7_string();
		String get_sv8_string();
		String get_sv9_string();
		String get_sv10_string();
		String get_sv11_string();
		String get_sv12_string();
		String get_time_string();
		String get_usep_string();
		String get_vdop_string();
		
		float get_altitude();
		float get_course();
		int get_nosat();
		int get_data_status();
		int get_fix_mode();
		int get_fix_status();
		int get_mode_ind();
		float get_speed(int speed_units = 0);
		
	private:
		// PRIVATE FUNCTIONS
		int parse_delimiter_indexes(String message_string, int index_array[]);
		void parse_field_substrings(String message_string, int index_array[], int num_fields, char *fields[]);
		void print_field_substrings(char* fields[], int label_number, int num_fields);
		int verify_checksum(String message);
		int convert_from_hex(char c);
		void initialize_nmea_fields();
		
		// PRIVATE VARIABLES
		// All available GPS NMEA Message Fields (in alphabetical order)     
		char   _alt[12],_altref[12],_altu[12],_cog[12],_cogm[12],_cs_gga[12],_cs_gsa[12],_cs_rmc[12],_cs_vtg[12],_date[12],_diffage[12],
			   _diffstation[12],_e_w[12],_fs[12],_hdop[12],_k[12],_latitude[12],_longitude[12],_m[12],_mode[12],_modeind[12],_mv[12],_mve[12],
			   _n[12],_n_s[12],_nosat[12],_pdop[12],_smode[12],_speed[12],_speed_kph[12],_status[12],_sv1[12],_sv2[12],_sv3[12],_sv4[12],
			   _sv5[12],_sv6[12],_sv7[12],_sv8[12],_sv9[12],_sv10[12],_sv11[12],_sv12[12],_t[12],_time[12],_usep[12],_vdop[12];

		// Arrays hold the particular fields present in each NMEA message type
		//   These arrays must be arranged with fields in the order the fields appear in that particular NMEA messags strin
		char *_GPGGA_fields[15];
		char *_GPGSA_fields[18];
		char *_GPRMC_fields[13];
		char *_GPVTG_fields[10];

		// Static  Arrays to hold the labels for the fields present in each NMEA message type
		static const char _GPGGA_labels[][15];
		static const char _GPGSA_labels[][15];
		static const char _GPRMC_labels[][15];
		static const char _GPVTG_labels[][15];

		// Arrays to hold the values of the indexes of the delimiters in each message type;
		int _GPGGA_indexes[15];
		int _GPGSA_indexes[18];
		int _GPRMC_indexes[13];
		int _GPVTG_indexes[10];
		
		// Character array (buffer) to hold the GPS input
		char _char_buff[90];
};

// 
// Latitude Object which contains internal private variables, degrees, minutes and seconds
//   To calculate the degrees, minute and seconds of latitude just pass the constructor
//   a String object representing a standard NMEA latitude that is 3,2.4 or 3,2.5 where
//   the string has 3 places for the integer degrees, 2 for the integer minutes and 4 or 5
//   after te decimal point for fractional minutes. By converting the NMEA string to degrees
//   minutes and seconds instead of just fractional degrees no loss of precision occurs due to
//   limited size of the Arduino float data type (which is only precise to 6 or 7 total figures,
//   that is 6 or 7 numbers total on both sides of the decimal point).
//
class Latitude {
  public:
	  Latitude(String latitude_string);			// Constructor, calculates degrees, minutes & seconds
	  int get_degrees();                        // returns integer number of degrees of latitude
	  int get_minutes();                        // returns integer number of minutes of latitude
	  float get_seconds();                      // returns float seconds

	private:
	  int _degrees;                         	// private variable contains number of degrees
	  int _minutes;                            	// private variable contains number of minutes
	  float _seconds;                          	// private variable contains number of seconds
};

// 
// Longitude Object which contains internal private variables, degrees, minutes and seconds
//   To calculate the degrees, minutes and seconds of longitude just pass the constructor
//   a String object representing a standard NMEA longitude that is 3,2.4 or 3,2.5 where
//   the string has 3 places for the integer degrees, 2 for the integer minutes and 4 or 5
//   after the decimal point for fractional minutes. By converting the NMEA string to degrees
//   minutes and seconds instead of just fractional degrees no loss of precision occurs due to
//   limited size of the Arduino float data type (which is only precise to 6 or 7 total figures,
//   that is 6 or 7 numbers total on both sides of the decimal point).
//
class Longitude {
  public:
	  Longitude(String latitude_string);   		// Constructor, calculates degrees, minutes & seconds
	  int get_degrees();                        // returns integer number of degrees of longitude
	  int get_minutes();                        // returns integer number of minutes of longitude
	  float get_seconds();                      // returns float seconds of longitude
  
  private:
	  int _degrees;                             // private variable contains number of degrees
	  int _minutes;                            	// private variable contains number of minutes
	  float _seconds;                           // private variable contains number of seconds
};

// 
// Date object which contains internal private variables: day, month, year and formatted date
//   To parse out the day, month and year of date just pass the constructor
//   a String object representing a standard NMEA UTC date that is 2,2,2 where
//   the string has 2 places for day, 2 for the month and 2 places for the year 
//
class Date {
  public:
	  Date(String Date_string);                 // Constructor, calculates day, month and year
	  int get_day();                            // returns integer day
	  int get_month();                          // returns integer month
	  int get_year();                           // returns integer year
	  String get_formatted_date();
  
  private:
	  int _day;                                 // private variable contains day
	  int _month;                               // private variable contains month
	  int _year;                                // private variable contains year
};

// 
// Time Object which contains internal private variables, hours, minutes, seconds and formatted time
//   To parse out the hours, minutes and seconds of Time just pass the constructor
//   a String object representing a standard NMEA UTC time that is 2,2,2.3 where
//   the string has 2 places for the integer hours, 2 for the integer minutes 5 places 2 places before 
//   and 3 after the decimal point for seconds.
//
class Time {
  public:
	  Time(String time_string);      			// Constructor, calculates hours, minutes & seconds
	  int get_hours();                          // returns integer number hours
	  int get_minutes();                        // returns integer number of minutes
	  float get_seconds();                      // returns float seconds
	  String get_formatted_time();
  
  private:
	  int _hours;                            	// private variable contains number of degrees
	  int _minutes;                             // private variable contains number of minutes
	  float _seconds;                         	// private variable contains number of seconds
	  String _formatted_time_string;            // private variable contains a formated string of the time
};
#endif